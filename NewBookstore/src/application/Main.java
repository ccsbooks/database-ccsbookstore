package application;
	
import java.util.ArrayList;
import java.util.List;

import controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.Address;
import model.Book;
import model.Genre;
import model.Payment;
import model.Publication;
import model.User;

public class Main extends Application {
	/**
	 * Declares a User in the main loader for the Javafx window
	 * to be used in the model
	 */
	private User user = new User();
	private Address address = new Address();
	private Payment payment = new Payment();
	private List<Publication> publishers = new ArrayList<>();
	private List<Book> book = new ArrayList<>();
	private List<Genre> genre = new ArrayList<>();
	
	/**
	 * @return the genre
	 */
	public final List<Genre> getGenre() {
		return genre;
	}
	/**
	 * @param genre the genre to set
	 */
	public final void setGenre(List<Genre> genre) {
		this.genre = genre;
	}
	/**
	 * @return the book
	 */
	public final List<Book> getBook() {
		return book;
	}
	/**
	 * @param book the book to set
	 */
	public final void setBook(List<Book> book) {
		this.book = book;
	}
	/**
	 * @return the publishers
	 */
	public final List<Publication> getPublishers() {
		return publishers;
	}
	/**
	 * @param publishers the publishers to set
	 */
	public final void setPublishers(List<Publication> publishers) {
		this.publishers = publishers;
	}
	/**
	 * This declares the "instance" of the window
	 */
	private static Main instance;
	/**
	 * Sets the instance as the main
	 */
	public Main() {
		instance = this;
	}
	/**
	 * This makes the instance retrievable from the controller
	 * @return
	 */
	public static Main getInstance() {
		return instance;
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			//this block of code loads the home page on run
			Parent root = FXMLLoader.load(getClass().getResource("../view/SignIn.fxml"));
			Scene scene = new Scene(root);
			primaryStage.setTitle("Sign In");
			primaryStage.getIcons().add(new Image("images/CCsBooks.png"));
			scene.getStylesheets().add(getClass().getResource("../style/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}

}
