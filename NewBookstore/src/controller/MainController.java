package controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Address;
import model.AddressDao;
import model.Book;
import model.BookDao;
//import model.Purchase;
//import model.PurchaseDao;
import model.CartItem;
import model.Genre;
import model.GenreDao;
import model.Invoice;
import model.Payment;
import model.PaymentDao;
import model.Phone;
import model.PhoneDao;
import model.Publication;
import model.PublicationDao;
import model.Purchase;
import model.ShoppingCartDao;
import model.User;
import model.UserDao;
import reports.Report;

public class MainController {
	private static final double DISCOUNT_AMOUNT = 75;
	private static final int DISCOUNT_AMOUNT_PERCENT = 10;
	private double totalPrice;
	private int cartQ;
	@FXML
	private AddressController addressController;
	@FXML
	private PaymentController paymentController;

	// ------------------Variables linked to the SignIn.fxml--------------------
	@FXML
	private TextField emailSignIn;
	@FXML
	private PasswordField passwordSignIn;

	// ------------------Variables linked to the MainMenu.fxml--------------------
	/**
	 * Grabs the button pressed from the main FXML file
	 */
	@FXML
	private Button btn;
	/**
	 * Variable that works with loading the secondary page inside the main page
	 */
	@FXML
	private AnchorPane centerRootPane;
	/**
	 * Variable that works with loading the secondary page inside the right side of
	 * the main page
	 */
	@FXML
	private AnchorPane rightRootPane;
	@FXML
	private Label title;
	@FXML
	private Button btnSignInMain;
	@FXML
	private Button btnEditAddress;
	@FXML
	private Button btnEditPayment;
	@FXML
	private Button btnCreatePayment;
	@FXML
	private Button btnUpdatePayment;
	@FXML
	private Button btnEditPhone;
	@FXML
	private FlowPane leftFlowPane;
	@FXML
	private Label welcomeLabel;
	@FXML
	private Button btnShoppingCart;

	// ---------------Variables linked to the book pages--------------
	// -------------Everything in the book is created from initialize
	@FXML
	private TextField txtISBN;
	@FXML
	private TextField txtBookName;
	@FXML
	private TextField txtAuthor;
	@FXML
	private TextField txtRetailPrice;
	@FXML
	private TextField txtBinding;
	@FXML
	private TextField txtQuantityOnHand;
	@FXML
	private TextField txtWholesale;
	@FXML
	private TextField txtPublicationDate;
	@FXML
	private ImageView bookImage;
	@FXML
	private GridPane bookGrid;
	@FXML
	private GridPane checkoutAddressGrid;
	@FXML
	private GridPane checkoutPaymentGrid;
	@FXML
	private TextField txtSearch;
	@FXML
	private Label lblBookName;
	@FXML
	private GridPane genreGrid;
	@FXML
	private Button bookButton;
	@FXML
	private AnchorPane AdminBook;
	@FXML
	private Tab AdminTabBook;
	@FXML
	private TabPane tabPane;
	@FXML
	private TabPane rightTabPane;
	@FXML
	private Label lblAddressLine1;
	@FXML
	private Label lblState;
	@FXML
	private Label lblPublisherName;
	@FXML
	private Label lblPublisherContactName;
	@FXML
	private Label lblPublisherNumber;
	@FXML
	private Label lblQuantity;
	@FXML
	private Label lblISBN;
	@FXML
	private Label lblBinding;
	@FXML
	private Label lblCopyrightDate;
	@FXML
	private Label lblRetailPrice;
	@FXML
	private Label lblAuthor;
	@FXML
	private Label lblZip;
	@FXML
	private Label lblCity;
	@FXML
	private Label lblCountry;
	@FXML
	private Label lblGenre;
	@FXML
	private GridPane BookInfoGrid;

	// ------------------Variables linked to the EditAddress.fxml----------------
	@FXML
	private TextField addressFirstName;
	@FXML
	private TextField addressLastName;
	@FXML
	private TextField addressLineOne;
	@FXML
	private TextField addressLineTwo;
	@FXML
	private TextField city;
	@FXML
	private TextField zip;
	@FXML
	private TextField state;
	@FXML
	private TextField country;
	@FXML
	private TextField txtFieldAddressID;
	@FXML
	private Button btnUpdateAddress;
	@FXML
	private Button btnCreateNewAddress;
	@FXML
	private Button btnDeleteAddress;
	@FXML
	private Button btnCancel;

	// -----------Variables for the AddressCardHolder.fxml-----
	@FXML
	private FlowPane addressCardHolder;
	@FXML
	private AnchorPane addressAnchor;

	// ---------------Variables for AddressCard.fxml------------
	@FXML
	private Label firstNameAddressCard;
	@FXML
	private Label lastNameAddressCard;
	@FXML
	private Label addressLineOneCard;
	@FXML
	private Label addressLineTwoCard;
	@FXML
	private Label cityStateZip;
	@FXML
	private Button btnEditAdd;
	@FXML
	private Button btnEditPay;
	@FXML
	private Button btnAddAdd;
	@FXML
	private Button btnAddPay;
	@FXML
	private GridPane addressGrid;
	@FXML
	private GridPane paymentGrid;

	List<GridPane> userGridAddresses = new ArrayList<>();

	// -----------------Variables for the payment sub page----
	@FXML
	private TextField cardNumber;
	@FXML
	private TextField cardType;
	@FXML
	private DatePicker expDate;
	@FXML
	private TextField cvv;
	@FXML
	private TextField cardActive;
	@FXML
	private TextField addressLineOnePay;
	@FXML
	private TextField addressLineTwoPay;
	@FXML
	private TextField cityPay;
	@FXML
	private TextField statePay;
	@FXML
	private TextField zipPay;
	@FXML
	private TextField countryPay;
	@FXML
	private TextField paymentFirstName;
	@FXML
	private TextField paymentLastName;
	@FXML
	private TextField txtFieldPaymentID;
	@FXML
	private Button btnCreatePaymentPane;
	@FXML
	private Button btnUpdatePaymentPane;
	@FXML
	private Button btnDeletePayment;
	@FXML
	private Button btnCancelPane;
	@FXML
	private Label lblPayment;

	// ---------------Variables for the edit profile page----
	@FXML
	private Button btnEditUser;
	@FXML
	private Button btnUpdateUser;
	@FXML
	private Button btnCancelEditUser;
	@FXML
	private TextField txtFldEditLastName;
	@FXML
	private TextField txtFldEditFirstName;
	@FXML
	private TextField txtFldEditEmail;
	@FXML
	private TextField txtFldEditPhone;
	@FXML
	private TextField pswdEditOldPassword;
	@FXML
	private TextField pswdEditNewPassword;
	@FXML
	private TextField pswdEditConfirmPassword;

	@FXML
	private AnchorPane topPane;
	// ----------------Variables for the Admin User page-----
	@FXML
	private TextField txtAdminPublisherZip;
	@FXML
	private TextField txtAdminPublisherAddress;
	@FXML
	private TextField txtAdminPublisherContact;
	@FXML
	private TextField txtAdminPublisherCountry;
	@FXML
	private TextField txtAdminPublisher;
	@FXML
	private TextField txtAdminPublisherCity;
	@FXML
	private TextField txtAdminPublisherState;
	@FXML
	private TextField txtAdminPublisherPhone;
	@FXML
	private TextField txtAdminPublisherPhoneType;

	// ------------------Variables for the Order Confirmation page-----
	@FXML
	private Label orderPayName;
	@FXML
	private Label orderCreditCard;
	@FXML
	private Label orderCardType;
	@FXML
	private Label orderAddName;
	@FXML
	private Label orderAddLineOne;
	@FXML
	private Label orderAddLineTwo;
	@FXML
	private Label orderCityStateZip;
	@FXML
	private Label orderCountry;

	private boolean checkBoxFlagAdd; // this variable is for before proceeding to the order confirmation page
	private boolean checkBoxFlagPay;

	// -----------------Variables for the publication page--------
	@FXML
	private ComboBox<String> adminPublicationDropdown;
	@FXML
	private ComboBox<String> adminPublicationDropdown2;
	@FXML
	private ComboBox<String> adminBookDropdown;
	@FXML
	private ComboBox<String> adminBookDropdownOut;
	@FXML
	private ComboBox<String> adminGenreDropdown;
	@FXML
	private TitledPane adminButtons;

	User myUser = new User();
	UserDao daoUser = new UserDao();
	Book myBook = new Book();
	BookDao daoBook = new BookDao();
	Address myAddress = new Address();
	AddressDao daoAddress = new AddressDao();
	Payment myPayment = new Payment();
	PaymentDao daoPayment = new PaymentDao();
	Phone myPhone = new Phone();
	PhoneDao daoPhone = new PhoneDao();
	Publication myPublication = new Publication();
	PublicationDao daoPublication = new PublicationDao();
	Genre myGenre = new Genre();
	GenreDao daoGenre = new GenreDao();
	Purchase purchase = new Purchase();
	Report myReports = new Report();

	@FXML
	private GridPane shoppingCartGrid;
	@FXML
	private Button btnAddToCart;
	@FXML
	private Button btnProceedToCheckout;
	@FXML
	private Label lblCartTotal;
	@FXML
	private Label lblNewCartTotal;
	@FXML
	private Label lblDiscountApplied;
	@FXML
	private Label lblTotal;
	@FXML
	private Label lblNewTotal;
	@FXML
	private GridPane AdminOrdersGrid;
	@FXML
	private GridPane orderDetailGrid;
	@FXML
	private Label lblCartWarning;
	@FXML
	private ImageView ccImg;

	// --------------------Method that initializes the book pages----
	@FXML
	private void initialize() {
		if (Main.getInstance().getUser().getAdmin() == 1) {
			adminButtons.setVisible(true);
		}
		List<CartItem> shoppingCartItems = new ArrayList<CartItem>();
		shoppingCartItems = daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID());
		cartQ = shoppingCartItems.size();
		btnShoppingCart.setText("Shopping Cart (" + cartQ + ")");

		// paymentController.injectMainController(this);
		// addressController.injectMainController(this);

		String name = Main.getInstance().getUser().getFirstName();
		welcomeLabel.setText("Welcome " + name);
		// this is to generate the genre buttons

		Search();
		generateAddresses();
		generatePayments();
	}

	void generateAddresses() {
		// ---------------------Code for Address tab-------------
		// this is to load up all the addresses by userID on the home page
		int addressInnerRow = 0;
		int addressInnerCol = 0;
		int addressRow = 1;
		int addressCol = 0;

		List<Address> addresses = new ArrayList<Address>();
		addresses = filteredAddresses();
		for (Address a : addresses) {
			if ((addressCol % 5) == 0 && addressCol != 0) {
				addressRow = addressRow + 3;
				addressCol = 0;
			}
			GridPane innerGridAdd = new GridPane();

			btnEditAdd = new Button("Edit");

			Label addressName = new Label();
			Label addressFirstLine = new Label();
			Label addressSecondLine = new Label();
			Label addressCityStateZip = new Label();
			Label addressCountry = new Label();
			addressGrid.add(innerGridAdd, addressCol, addressRow);
			innerGridAdd.setStyle("-fx-grid-lines-visible: true");

			addressName.setText(" " + a.getAddressFirstName() + " " + a.getAddressLastName());
			innerGridAdd.add(addressName, addressInnerCol, addressInnerRow + 2);
			addressFirstLine.setText(" " + a.getAddressLineOne());
			innerGridAdd.add(addressFirstLine, addressInnerCol, addressInnerRow + 3);

			int n1 = 0;
			if (!a.getAddressLineTwo().equals("")) {
				addressSecondLine.setText(" " + a.getAddressLineTwo());
				n1 = 1;
				innerGridAdd.add(addressSecondLine, addressInnerCol, addressInnerRow + 4);
			} else if (a.getAddressLineTwo().equals("")) {
				n1 = 0;
			}

			addressCityStateZip.setText(" " + a.getCity() + ", " + a.getState() + " " + a.getZip());
			innerGridAdd.add(addressCityStateZip, addressInnerCol, addressInnerRow + 4 + n1);
			addressCountry.setText(" " + a.getCountry());
			innerGridAdd.add(addressCountry, addressInnerCol, addressInnerRow + 5 + n1);

			innerGridAdd.add(btnEditAdd, addressInnerCol, addressInnerRow + 6 + n1);
			btnEditAdd.setId(String.valueOf(a.getAddressID()));
			// adds all of the users addresses to an array so they can be
			// accessed by another tab pane
			userGridAddresses.add(innerGridAdd);
			btnEditAdd.setOnAction((event) -> {
				getAddress(a.getAddressID());
			});

			addressInnerCol = addressInnerCol + 1;
			addressCol = addressCol + 1;
		}

		// This block of code generates a button after all of
		// the addresses
		if ((addressCol % 5) == 0 && addressCol != 0) {
			addressRow = addressRow + 3;
			addressCol = 0;
		}
		GridPane newAddressInnerGrid = new GridPane();
		btnAddAdd = new Button("+");
		btnAddAdd.setId("add-address-btn");
		addressGrid.add(newAddressInnerGrid, addressCol, addressRow);
		newAddressInnerGrid.add(btnAddAdd, 0, 0);
		btnAddAdd.setOnAction((event) -> {
			newAddressTab();
		});
	}

	void generatePayments() {
		// ---------------------Code for Payment tab-------------
		// this is to load up all the payment methods by userID on the home page
		int paymentInnerRow = 0;
		int paymentInnerCol = 0;
		int paymentRow = 1;
		int paymentCol = 0;

		List<Payment> allUsersPayments = new ArrayList<Payment>();
		allUsersPayments = daoPayment.getAllbyID(Main.getInstance().getUser().getUserID());
		for (Payment p : allUsersPayments) {
			if ((paymentCol % 5) == 0 && paymentCol != 0) {
				paymentRow = paymentRow + 3;
				paymentCol = 0;
			}
			GridPane innerGridPay = new GridPane();

			btnEditPay = new Button("Edit");

			Label cardNumber = new Label();
			Label expDate = new Label();
			Label cvv = new Label();
			Label cardType = new Label();
			paymentGrid.add(innerGridPay, paymentCol, paymentRow);
			innerGridPay.setStyle("-fx-grid-lines-visible: true");

			cardNumber.setText(" xxxx-xxxx-xxxx-" + p.getLastFour());
			innerGridPay.add(cardNumber, paymentInnerCol, paymentInnerRow + 0);
			expDate.setText(" " + p.getExpDate());
			innerGridPay.add(expDate, paymentInnerCol, paymentInnerRow + 1);
			cvv.setText(" " + p.getCvv());
			innerGridPay.add(cvv, paymentInnerCol, paymentInnerRow + 2);
			cardType.setText(" " + p.getCardType());
			innerGridPay.add(cardType, paymentInnerCol, paymentInnerRow + 3);

			innerGridPay.add(btnEditPay, paymentInnerCol, paymentInnerRow + 4);
			btnEditPay.setId(String.valueOf(p.getPaymentID()));
			btnEditPay.setOnAction((event) -> {
				getPayment(p.getPaymentID());
			});

			paymentInnerCol = paymentInnerCol + 1;
			paymentCol = paymentCol + 1;
		}

		// This block of code generates a button after all of
		// the payments
		if ((paymentCol % 5) == 0 && paymentCol != 0) {
			paymentRow = paymentRow + 3;
			paymentCol = 0;
		}
		GridPane newPaymentInnerGrid = new GridPane();
		btnAddPay = new Button("+");
		btnAddPay.setId("add-address-btn");
		paymentGrid.add(newPaymentInnerGrid, paymentCol, paymentRow);
		newPaymentInnerGrid.add(btnAddPay, 0, 0);
		btnAddPay.setOnAction((event) -> {
			newPaymentTab();
		});
	}

	@FXML
	void FunTime() {
		topPane.setRotate(Math.random() * 360);
	}

	@FXML
	void ResetFunTime() {
		topPane.setRotate(0);
	}

	// ----------Search for books--------
	@FXML
	void Search() {
		cancelAddress();
		closeEditPayment();
		int innerRow = 0;
		int innerCol = 0;
		int row = 1;
		int col = 0;
		bookGrid.getChildren().clear();

		for (Book e : daoBook.searchAll(txtSearch.getText())) {

			GridPane innerGrid = new GridPane();
			ImageView bookImage = new ImageView();
			bookButton = new Button("View");
			Image img;
			try {
				img = new Image("images/" + e.getBookID() + ".jpg");
			} catch (Exception ee) {
				img = new Image("images/0.jpg");
			}
			HBox myHBox = new HBox();

			Label bookName = new Label();
			Label bookAuthor = new Label();
			Label bookRetailPrice = new Label();
			bookGrid.add(innerGrid, col, row);
			innerGrid.setStyle("-fx-grid-lines-visible: true; -fx-border-radius: 10; -fx-alignment: center;");

			bookImage.setFitHeight(200);
			bookImage.setFitWidth(200);
			bookImage.setImage(img);
			innerGrid.add(bookImage, innerCol, innerRow);

			bookName.setText(" " + e.getBookName());
			bookName.setMaxWidth(210);
			innerGrid.add(bookName, innerCol, innerRow + 2);
			bookAuthor.setText(" By " + e.getAuthor());
			bookAuthor.setMaxWidth(210);
			innerGrid.add(bookAuthor, innerCol, innerRow + 3);

			DecimalFormat df2 = new DecimalFormat(".00");
			bookRetailPrice.setText(" $" + df2.format(e.getRetailPrice()));

			innerGrid.add(bookRetailPrice, innerCol, innerRow + 4);
			innerGrid.add(myHBox, innerCol, innerRow + 5);

			myHBox.getChildren().add(bookButton);
			myHBox.setStyle("-fx-alignment: center;");
			bookButton.setStyle("-fx-padding: 5 35 5 35;");
			bookButton.setId(String.valueOf(e.getBookID()));
			int n = e.getBookID();
			bookButton.setOnAction((event) -> {
				GetBook(n);
			});

			innerCol = innerCol + 1;
			col = col + 2;

			// bookGrid.setMinHeight(row * 130);
			bookGrid.setMinWidth(750);

			if ((col % 3) == 0 && col != 0) {
				Label blah = new Label();
				row = row + 1;
				bookGrid.add(blah, col, row);
				row = row + 1;
				col = 0;
			}

			tabPane.getSelectionModel().select(1);
		}
	}

	@FXML
	void BestSellers() {
		cancelAddress();
		closeEditPayment();
		int innerRow = 0;
		int innerCol = 0;
		int row = 1;
		int col = 0;
		bookGrid.getChildren().clear();

		for (Book e : daoBook.searchAllByBestSellers()) {
			GridPane innerGrid = new GridPane();
			ImageView bookImage = new ImageView();
			bookButton = new Button("View");
			Image img;
			try {
				img = new Image("images/" + e.getBookID() + ".jpg");
			} catch (Exception ee) {
				img = new Image("images/0.jpg");
			}
			HBox myHBox = new HBox();

			Label bookName = new Label();
			Label bookAuthor = new Label();
			Label bookRetailPrice = new Label();
			bookGrid.add(innerGrid, col, row);
			innerGrid.setStyle("-fx-grid-lines-visible: true; -fx-border-radius: 10; -fx-alignment: center;");

			bookImage.setFitHeight(200);
			bookImage.setFitWidth(200);
			bookImage.setImage(img);
			innerGrid.add(bookImage, innerCol, innerRow);

			bookName.setText(" " + e.getBookName());
			bookName.setMaxWidth(210);
			innerGrid.add(bookName, innerCol, innerRow + 2);
			bookAuthor.setText(" By " + e.getAuthor());
			bookAuthor.setMaxWidth(210);
			innerGrid.add(bookAuthor, innerCol, innerRow + 3);

			DecimalFormat df2 = new DecimalFormat(".00");
			bookRetailPrice.setText(" $" + df2.format(e.getRetailPrice()));

			innerGrid.add(bookRetailPrice, innerCol, innerRow + 4);
			innerGrid.add(myHBox, innerCol, innerRow + 5);
			myHBox.getChildren().add(bookButton);
			myHBox.setStyle("-fx-alignment: center;");
			bookButton.setStyle("-fx-padding: 5 35 5 35;");
			bookButton.setId(String.valueOf(e.getBookID()));
			int n = e.getBookID();
			bookButton.setOnAction((event) -> {
				GetBook(n);
			});

			innerCol = innerCol + 1;
			col = col + 2;

			// bookGrid.setMinHeight(row * 130);
			bookGrid.setMinWidth(750);

			if ((col % 3) == 0 && col != 0) {
				Label blah = new Label();
				row = row + 1;
				bookGrid.add(blah, col, row);
				row = row + 1;
				col = 0;

			}

			tabPane.getSelectionModel().select(1);
		}

	}

	@FXML
	void addressTab() {
		cancelAddress();
		closeEditPayment();
		tabPane.getSelectionModel().select(3);

	}

	@FXML
	void startTab() {
		rightTabPane.getSelectionModel().select(0);
	}

	@FXML
	void openBooksTab() {
		cancelAddress();
		closeEditPayment();
		txtSearch.setText("");
		Search();
	}

	@FXML
	void newAddressTab() {
		rightTabPane.getSelectionModel().select(2);
		clearEditAddressTextFields();
		btnDeleteAddress.setVisible(false);
		btnCreateNewAddress.setVisible(true);
		btnUpdateAddress.setVisible(false);
	}

	@FXML
	void newPaymentTab() {
		rightTabPane.getSelectionModel().select(1);
		btnDeletePayment.setVisible(false);
		btnCreatePaymentPane.setVisible(true);
		btnUpdatePaymentPane.setVisible(false);
		clearTextFieldsForPaymentPane();
	}

	@FXML
	void paymentsTab() {
		cancelAddress();
		closeEditPayment();
		tabPane.getSelectionModel().select(5);
		clearTextFieldsForPaymentPane();
	}

	@FXML
	void editProfileTab() {
		cancelAddress();
		closeEditPayment();
		tabPane.getSelectionModel().select(8);
		myUser = Main.getInstance().getUser();

		txtFldEditLastName.setText(myUser.getLastName());
		txtFldEditFirstName.setText(myUser.getFirstName());
		txtFldEditEmail.setText(myUser.getEmail());
		txtFldEditPhone.setText(myUser.getPhone().getPhoneNumber());

		txtFldEditLastName.setDisable(true);
		txtFldEditFirstName.setDisable(true);
		txtFldEditEmail.setDisable(true);
		txtFldEditPhone.setDisable(true);

		btnEditUser.setVisible(true);
		btnCancelEditUser.setVisible(false);
		btnUpdateUser.setVisible(false);

	}

	@FXML
	void editUser(ActionEvent event) {
		txtFldEditLastName.setDisable(false);
		txtFldEditFirstName.setDisable(false);
		txtFldEditEmail.setDisable(false);
		txtFldEditPhone.setDisable(false);

		btnCancelEditUser.setVisible(true);
		btnEditUser.setVisible(false);
		btnUpdateUser.setVisible(true);
	}

	@FXML
	void cancelEditUser(ActionEvent event) {
		editProfileTab();
	}

	@FXML
	void updateUser(ActionEvent event) {
		myUser = Main.getInstance().getUser();
		// int w = 0;
		//
		// if (txtFldEditLastName.getText().equals(null))
		// ;
		// w++;
		// if (txtFldEditFirstName.getText().equals(null))
		// ;
		// w++;
		// if (txtFldEditEmail.getText().equals(null))
		// ;
		// w++;
		//
		// if (w > 0) {
		// Alert alert = new Alert(AlertType.ERROR);
		// alert.setTitle("Missing Information");
		// alert.setHeaderText("You are missing some information in this form.");
		// alert.setContentText("There are text fields that require information before
		// this form can be submitted.");
		// alert.showAndWait();
		// } else {
		myUser.setLastName(txtFldEditLastName.getText());
		myUser.setFirstName(txtFldEditFirstName.getText());
		myUser.setEmail(txtFldEditEmail.getText());
		myUser.getPhone().setPhoneNumber(txtFldEditPhone.getText());
		// TODO:have to hash password
		daoUser.update(myUser);
		editProfileTab();
		// }

	}

	@FXML
	void shoppingCartTab() {
		cancelAddress();
		closeEditPayment();
		clearGridPanes();
		// TODO:finnd out what this is for
		// add refresh for the addresses and payment.
		tabPane.getSelectionModel().select(6);
		List<CartItem> shoppingCartItems = new ArrayList<CartItem>();
		shoppingCartItems = daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID());
		Main.getInstance().getUser().setShoppingCart(shoppingCartItems);
		System.out.println(Main.getInstance().getUser().getShoppingCart());
		
		btnShoppingCart.setText("Shopping Cart (" + shoppingCartItems.size() + ")");

		// code to control the appearance of the checkout button
		if (shoppingCartItems.size() > 0) {
			btnProceedToCheckout.setVisible(true);
			lblCartWarning.setVisible(false);
		}
		if (shoppingCartItems.size() == 0) {
			btnProceedToCheckout.setVisible(false);
			lblNewCartTotal.setVisible(false);
			lblNewTotal.setVisible(false);
			lblTotal.setVisible(false);
			lblCartTotal.setVisible(false);
			lblDiscountApplied.setVisible(false);
			lblCartWarning.setVisible(true);
		}

		// ---------------------Code for the Shopping Cart tab-------------
		// this is to load up all the shopping cart items by userID on the home page
		int innerRow = 0;
		int innerCol = 0;
		int row = 1;
		int col = 0;

		List<CartItem> usersItems = new ArrayList<CartItem>();
		usersItems = daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID());
		totalPrice = 0;
		for (CartItem sc : usersItems) {
			GridPane innerGridSc = new GridPane();
			ImageView bookImage = new ImageView();
			Image img;
			try {
				img = new Image("images/" + sc.getBookID() + ".jpg");
			} catch (Exception ee) {
				img = new Image("images/0.jpg");
			}
			
			// based off of the book ID, grab the info about the book:
			BookDao bDao = new BookDao();
			Book book = new Book();
			book = bDao.get(sc.getBookID());

			List<Integer> booksOnHand = new ArrayList<Integer>();

			int n;
			for (n = 1; n <= book.getQuantityOnHand(); ++n) {
				booksOnHand.add(n);
			}

			Button btnEditCartItem = new Button("Edit");
			Button btnRemoveCartItem = new Button("Remove from Cart");
			Button btnSaveCartItem = new Button("Save Changes");

			HBox hbox = new HBox();
			Label lblQuantity = new Label("Quantity: ");
			Label bookTitle = new Label();
			Label bookAuthor = new Label();
			Label price = new Label();
			Label totalPriceLbl = new Label();
			ChoiceBox<Integer> quantity = new ChoiceBox<Integer>();
			quantity.setDisable(true);
			shoppingCartGrid.add(innerGridSc, col, row);
			innerGridSc.setStyle("-fx-grid-lines-visible: true");
			quantity.setStyle("-fx-opacity: 1.0;");

			bookImage.setFitHeight(200);
			bookImage.setFitWidth(200);
			bookImage.setImage(img);
			innerGridSc.add(bookImage, innerCol, innerRow +2);
			bookTitle.setText(" " + book.getBookName());
			innerGridSc.add(bookTitle, innerCol, innerRow + 3);

			bookAuthor.setText(" " + book.getAuthor());
			innerGridSc.add(bookAuthor, innerCol, innerRow + 4);

			int onHand = book.getQuantityOnHand();
			int inCart = sc.getQuantity();
			if (inCart > onHand) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Quantity Has Changed");
				alert.setContentText("The quantity on hand has changed for " + book.getBookName()
						+ ". The quantity in your cart will reflect this change.");
				alert.showAndWait();
				quantity.getItems().addAll(booksOnHand);
				quantity.getSelectionModel().select(onHand - 1);
				sc.setQuantity(quantity.getSelectionModel().getSelectedIndex() + 1);

				UserDao uDao = new UserDao();
				uDao.updateUserCartItem(sc);
			} else {
				quantity.getItems().addAll(booksOnHand);
				quantity.getSelectionModel().select(sc.getQuantity() - 1);
			}
			DecimalFormat df2 = new DecimalFormat(".00");
			price.setText(String.valueOf(" $" + df2.format(book.getRetailPrice())));
			innerGridSc.add(price, innerCol, innerRow + 5);

			hbox.getChildren().add(lblQuantity);
			hbox.getChildren().add(quantity);
			innerGridSc.add(hbox, innerCol, innerRow + 6);

			totalPriceLbl.setText((" $" + String.valueOf(df2.format(book.getRetailPrice() * sc.getQuantity()))));
			innerGridSc.add(totalPriceLbl, innerCol, innerRow + 7);

			innerGridSc.add(btnEditCartItem, innerCol, innerRow + 8);
			btnEditCartItem.setId(String.valueOf(sc.getBookID()));
			btnEditCartItem.setOnAction((event) -> {
				btnEditCartItem.setVisible(false);
				quantity.setDisable(false);
				btnRemoveCartItem.setVisible(false);
				btnSaveCartItem.setVisible(true);
				btnShoppingCart.setText("Shopping Cart (" + --cartQ + ")");
			});

			innerGridSc.add(btnRemoveCartItem, innerCol, innerRow + 9);
			btnRemoveCartItem.setId(String.valueOf(sc.getBookID()));
			btnRemoveCartItem.setOnAction((event) -> {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirmation Dialog");
				alert.setHeaderText("Confirm deletion");
				alert.setContentText("Are you sure you want to delete this item from your shopping cart?");
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) {
					UserDao uDao = new UserDao();
					uDao.deleteUserCartItem(sc);
					// since I am sending all of the shopping cart items from the
					// model to the view, I have to delete them from the model
					// as well as deleting them from the view, so that is accurately
					// reflected in the model
					shoppingCartTab();
				} else {
				}
			});

			innerGridSc.add(btnSaveCartItem, innerCol, innerRow + 8);
			btnSaveCartItem.setVisible(false);
			btnSaveCartItem.setId(String.valueOf(sc.getBookID()));
			btnSaveCartItem.setOnAction((event) -> {
				// This action will save the new quantity to the database. This will be limited
				// to the
				// number of items that are actually in stock.
				quantity.setDisable(true);
				btnEditCartItem.setVisible(true);
				btnSaveCartItem.setVisible(false);
				btnRemoveCartItem.setVisible(true);
				sc.setQuantity(quantity.getSelectionModel().getSelectedIndex() + 1);

				UserDao uDao = new UserDao();
				uDao.updateUserCartItem(sc);
				shoppingCartTab();
			});
			totalPrice = (totalPrice + ((sc.getQuantity()) * (daoBook.get(sc.getBookID()).getRetailPrice())));
			if (totalPrice >= 75.00) {
				double oldPrice = totalPrice;
				double newPrice = totalPrice;
				lblTotal.setText(" $" + df2.format(oldPrice));
				lblTotal.setStyle("-fx-strikethrough: true;");
				newPrice = newPrice - (newPrice * 0.10);
				lblNewTotal.setText(" $" + df2.format(newPrice));
				lblCartTotal.setVisible(true);
				lblTotal.setVisible(true);
				lblNewCartTotal.setVisible(true);
				lblNewTotal.setVisible(true);
				lblDiscountApplied.setVisible(true);
			} else {
				// purchase.setTotalPrice(totalPrice);
				lblCartTotal.setVisible(true);
				lblTotal.setVisible(true);
				lblNewCartTotal.setVisible(false);
				lblNewTotal.setVisible(false);
				lblDiscountApplied.setVisible(false);
				lblTotal.setText(" $" + df2.format(totalPrice));
			}

			innerCol = innerCol + 1;
			col = col + 2;
			
			if ((col % 3) == 0 && col != 0) {
				Label blah = new Label();
				row = row + 1;
				shoppingCartGrid.add(blah, col, row);
				row = row + 1;
				col = 0;
			}
		}

	}

	@FXML
	void checkoutTab() {
		rightTabPane.getSelectionModel().select(0);
		tabPane.getSelectionModel().select(7);
		generateAddressesAndPaymentsForCheckout();

	}

	void generateAddressesAndPaymentsForCheckout() {
		checkBoxFlagAdd = false;
		checkBoxFlagPay = false;
		// this tab has the methods to load the payment methods and the shipping
		// addresses
		// for an order.
		int innerRowAdd = 0;
		int rowAdd = 0;
		int colAdd = 0;
		// this r variable is for adding a new row to the grid pane
		// every time a new address line is created.
		List<CheckBox> checkBoxesListAdd = new ArrayList<CheckBox>();
		String checkBoxIdAdd = null;
		int r = 4;
		for (Address a : filteredAddresses()) {
			// this generates a new grid pane inside the larger grid.
			HBox innerContainer = new HBox();
			// this will generate all of the nodes necessary for information and
			// the general functionality of the inner grids.
			Label addressName = new Label();
			Label addressFirstLine = new Label();
			Label addressSecondLine = new Label();
			Label cityStateZip = new Label();
			Label country = new Label();

			// the check box will be for letting the user decide which shipping address to
			// use amongst the different options.
			CheckBox checkBox = new CheckBox();
			// the following integer variable is for keeping track of the number of columns
			// in an inner grid.

			innerContainer.getChildren().add(checkBox);
			Pane pane = new Pane();
			checkoutAddressGrid.addRow(++r, pane);
			checkoutAddressGrid.add(innerContainer, colAdd, rowAdd);
			innerContainer.setStyle("-fx-grid-lines-visible: true; -fx-border-radius: 5;");

			addressName.setText(" " + a.getAddressLastName() + ", " + a.getAddressFirstName());
			innerContainer.getChildren().add(addressName);
			addressFirstLine.setText(" " + a.getAddressLineOne());
			innerContainer.getChildren().add(addressFirstLine);
			// if there is no second address in the database then the inner grid
			// will not contain one.
			if (!a.getAddressLineTwo().equals("")) {
				addressSecondLine.setText(" " + a.getAddressLineTwo());
				innerContainer.getChildren().add(addressSecondLine);
			} else if (a.getAddressLineTwo().equals("")) {
			}
			cityStateZip.setText(" " + a.getCity() + ", " + a.getState() + " " + a.getZip());
			innerContainer.getChildren().add(cityStateZip);
			country.setText(" " + a.getCountry());
			innerContainer.getChildren().add(country);
			// the following code creates the check box for customers to confirm what
			// shipping address
			// they want
			checkBoxIdAdd += a.getAddressID();
			checkBox.setId(checkBoxIdAdd);
			checkBoxesListAdd.add(checkBox);
			// this event handler for each choice box un-selects the other
			// choice boxes when you select one.
			checkBox.setOnAction((event) -> {
				for (CheckBox cb : checkBoxesListAdd) {
					if (!cb.getId().equals(checkBox.getId())) {
						cb.setSelected(false);
					}
				}
				purchase.setPurchAddress(a);
				checkBoxFlagAdd = true;
				// this block of code at the end of the event
				// makes one final check to see if a check box has
				// been selected and then if it hasn't it sets the
				// boolean to false
				int flag = 0;
				for (CheckBox cb : checkBoxesListAdd) {
					if (cb.isSelected() || checkBox.isSelected()) {
						flag++;
					}
				}
				if (flag == 0) {
					checkBoxFlagAdd = false;
				}

			});

			innerRowAdd = innerRowAdd + 1;
			rowAdd = rowAdd + 1;

		}

		Button btnNewAddress = new Button("Create a new shipping address");
		checkoutAddressGrid.add(btnNewAddress, colAdd, rowAdd);
		btnNewAddress.setOnAction((event) -> {
			clearGridPanes();
			checkoutTab();
			newAddressTab();
		});

		int innerRowPay = 0;
		int rowPay = 0;
		int colPay = 0;
		// this r variable is for adding a new row to the grid pane
		// every time a new address line is created.
		List<CheckBox> checkBoxesListPay = new ArrayList<CheckBox>();
		String checkBoxIdPay = null;
		int rp = 4;
		for (Payment p : daoPayment.getAllbyID(Main.getInstance().getUser().getUserID())) {
			// this generates a new grid pane inside the larger grid.
			HBox innerContainer = new HBox();
			// this will generate all of the nodes necessary for information and
			// the general functionality of the inner grids.
			Label cardNumber = new Label();
			Label cardType = new Label();
			Label expDate = new Label();

			// the check box will be for letting the user decide which payment to
			// use amongst the different options.
			CheckBox checkBox = new CheckBox();
			// the following integer variable is for keeping track of the number of columns
			// in an inner grid.

			innerContainer.getChildren().add(checkBox);
			Pane pane = new Pane();
			checkoutPaymentGrid.addRow(++rp, pane);
			checkoutPaymentGrid.add(innerContainer, colPay, rowPay);
			innerContainer.setStyle("-fx-grid-lines-visible: true; -fx-border-radius: 5");

			cardNumber.setText(" xxxx-xxxx-xxxx-" + p.getLastFour());
			innerContainer.getChildren().add(cardNumber);
			cardType.setText(" " + p.getCardType());
			innerContainer.getChildren().add(cardType);
			expDate.setText(" " + p.getExpDate());
			innerContainer.getChildren().add(expDate);
			// the following code creates the check box for customers to confirm what
			// shipping address
			// they want
			checkBoxIdPay += p.getPaymentID();
			checkBox.setId(checkBoxIdPay);
			checkBoxesListPay.add(checkBox);
			// this event handler for each choice box un-selects the other
			// choice boxes when you select one.
			checkBox.setOnAction((event) -> {
				for (CheckBox cb : checkBoxesListPay) {
					if (!cb.getId().equals(checkBox.getId())) {
						cb.setSelected(false);
					}
				}
				purchase.setPurchPayment(p);
				checkBoxFlagPay = true;
				// this block of code at the end of the event
				// makes one final check to see if a check box has
				// been selected and then if it hasn't it sets the
				// boolean to false
				int flag = 0;
				for (CheckBox cb : checkBoxesListPay) {
					if (cb.isSelected() || checkBox.isSelected()) {
						flag++;
					}
				}
				if (flag == 0)
					checkBoxFlagPay = false;

				Purchase purchase = new Purchase();
				purchase.setPurchPayment(p);

			});

			innerRowPay = innerRowPay + 1;
			rowPay = rowPay + 1;

		}
		Button btnNewPayment = new Button("Create a new payment method");
		checkoutPaymentGrid.add(btnNewPayment, colPay, rowPay);
		btnNewPayment.setOnAction((event) -> {
			clearGridPanes();
			checkoutTab();
			newPaymentTab();
		});
	}

	@FXML
	void returnToCart() {
		cancelAddress();
		closeEditPayment();
		clearGridPanes();
		shoppingCartTab();
	}

	@FXML
	void returnToCheckout() {
		clearGridPanes();
		checkoutTab();
	}

	@FXML
	void proceedToOrderConfirmation() {
		cancelAddress();
		closeEditPayment();
		if (checkBoxFlagAdd == true && checkBoxFlagPay == true) {
			clearGridPanes();
			List<CartItem> sc = new ArrayList<CartItem>();
			sc = Main.getInstance().getUser().getShoppingCart();
			DecimalFormat df2 = new DecimalFormat(".00");
			int column = 0;
			int row = 0;
			int n = 0;
			for (CartItem cartItem : sc) {
				n = 0;
				Label bookTitle = new Label();
				Label bookAuthor = new Label();
				Label price = new Label();
				Label quantity = new Label();
				Label totalPrice = new Label();

				Book book = new Book();
				book = daoBook.get(cartItem.getBookID());
				bookTitle.setText(book.getBookName());
				orderDetailGrid.add(bookTitle, column, row);
				bookAuthor.setText(book.getAuthor());
				orderDetailGrid.add(bookAuthor, column + ++n, row);
				price.setText("$" + String.valueOf(book.getRetailPrice()));
				orderDetailGrid.add(price, column + ++n, row);
				quantity.setText(String.valueOf(cartItem.getQuantity()));
				orderDetailGrid.add(quantity, column + ++n, row);
				totalPrice.setText(
						String.valueOf(("$" + df2.format((cartItem.getQuantity()) * (book.getRetailPrice())))));
				orderDetailGrid.add(totalPrice, column + ++n, row);
				row = row + 1;

			}
			Label totalLabel = new Label("Total");
			Label total = new Label("$" + String.valueOf(df2.format(totalPrice)));

			double discountNum = 0.00;
			if (totalPrice >= 75.00) {
				double difference;
				double newPrice;
				newPrice = totalPrice - (totalPrice * .10);
				difference = totalPrice - newPrice;
				discountNum = difference;
				totalPrice = newPrice;
			}

			Label discountLabel = new Label("Discount Applied");
			Label discount = new Label("- " + String.valueOf(df2.format(discountNum)));
			Label newTotalLabel = new Label("New Total");
			Label newTotal = new Label("$" + String.valueOf(df2.format(totalPrice)));

			orderDetailGrid.add(totalLabel, 0, row);
			orderDetailGrid.add(total, 4, row);
			orderDetailGrid.add(discountLabel, 0, ++row);
			orderDetailGrid.add(discount, 4, row);
			orderDetailGrid.add(newTotalLabel, 0, ++row);
			orderDetailGrid.add(newTotal, 4, row);

			tabPane.getSelectionModel().select(11);
			// set the payment method info
			orderPayName.setText(daoAddress.get(purchase.getPurchPayment().getAddressID()).getAddressLastName() + ", "
					+ daoAddress.get(purchase.getPurchPayment().getAddressID()).getAddressFirstName());
			orderCreditCard.setText("xxxx-xxxx-xxxx-" + purchase.getPurchPayment().getLastFour());
			orderCardType.setText(purchase.getPurchPayment().getCardType());

			// set the shipping address info
			orderAddName.setText(purchase.getPurchAddress().getAddressLastName() + ", "
					+ purchase.getPurchAddress().getAddressFirstName());
			orderAddLineOne.setText(purchase.getPurchAddress().getAddressLineOne());
			if (purchase.getPurchAddress().getAddressLineTwo().equals("")) {
				orderAddLineTwo.setPrefSize(0, 0);
			} else {
				orderAddLineTwo.setText(purchase.getPurchAddress().getAddressLineTwo());
			}
			orderCityStateZip.setText(purchase.getPurchAddress().getCity() + ", "
					+ purchase.getPurchAddress().getState() + " " + purchase.getPurchAddress().getZip());
			orderCountry.setText(purchase.getPurchAddress().getCountry());
		} else if (checkBoxFlagAdd == false || checkBoxFlagPay == false) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Select Option(s) Before Continuing");
			alert.setContentText("Ooops, select a a shipping address and payment method before continuing.");
			alert.showAndWait();
		}

	}

	@FXML
	void PlaceOrder() {

		int discount = 0;
		if (totalPrice >= DISCOUNT_AMOUNT) {
			discount = DISCOUNT_AMOUNT_PERCENT;
		}
		ShoppingCartDao cartDao = new ShoppingCartDao();

		// System.out.println(Main.getInstance().getUser().getUserID()
		// +","+ purchase.getPurchPayment().getPaymentID()
		// +","+ purchase.getPurchAddress().getAddressID()
		// +","+ discount);

		try {
			int purchaseID = cartDao.placeOrder(Main.getInstance().getUser().getUserID(),
					purchase.getPurchPayment().getPaymentID(), purchase.getPurchAddress().getAddressID(), discount);
			lblCartTotal.setText("0");
			tabPane.getSelectionModel().select(1);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Great Job");
			alert.setHeaderText("Order Confirmed");
			alert.setContentText("Click OK to save an order invoice.");
			myReports.GenerateInvoice(purchaseID);
			alert.showAndWait();
			tabPane.getSelectionModel().select(1);
		} catch (SQLException e) {
			if (e.getSQLState().equals("45000")) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Not enough in stock");
				alert.setContentText("There are not enough books in stock.");
				alert.showAndWait();
			} else {
				e.printStackTrace();
			}

		}

	}

	@FXML
	void AdminLoadPublication() {
		if (adminPublicationDropdown.getSelectionModel().getSelectedIndex() >= 0) {
			myPublication = daoPublication.get(Main.getInstance().getPublishers()
					.get(adminPublicationDropdown.getSelectionModel().getSelectedIndex()).getPublicationID());
			// Publication
			txtAdminPublisher.setText(myPublication.getPublisherName());
			txtAdminPublisherContact.setText(myPublication.getContactName());
			// This is for the contact phone number

			myPhone = daoPhone.get(myPublication.getPhoneID());
			// This sets the address
			myAddress = daoAddress.get(myPublication.getAddressID());

			// Publication
			txtAdminPublisher.setText(myPublication.getPublisherName());
			txtAdminPublisherContact.setText(myPublication.getContactName());
			txtAdminPublisherPhone.setText(myPhone.getPhoneNumber());
			txtAdminPublisherPhoneType.setText(myPhone.getPhoneType());
			txtAdminPublisherAddress.setText(myAddress.getAddressLineOne());
			txtAdminPublisherCity.setText(myAddress.getCity());
			txtAdminPublisherState.setText(myAddress.getState());
			txtAdminPublisherZip.setText(myAddress.getZip());
			txtAdminPublisherCountry.setText(myAddress.getCountry());
		}
	}

	@FXML
	void PrintOrderCSV() {
		// myReports.getAllInvoice();
	}

	@FXML
	void AdminLoadGenre() {

	}

	void AdminLoadBookDropdown() {
		// TODO: do it bra
	}

	@FXML
	void EditBookTab() {
		cancelAddress();
		closeEditPayment();
		// Get all publishers
		Main.getInstance().setPublishers(daoPublication.getALL());
		// TODO: get this finished
		adminPublicationDropdown.getItems().clear();
		adminPublicationDropdown2.getItems().clear();

		for (Publication p : Main.getInstance().getPublishers()) {

			adminPublicationDropdown.getItems().add(p.getPublisherName());
			adminPublicationDropdown2.getItems().add(p.getPublisherName());
		}

		// get all books
		// get all books

		Main.getInstance().setBook(daoBook.getALL());
		adminBookDropdown.getItems().clear();
		for (Book e : Main.getInstance().getBook()) {
			adminBookDropdown.getItems().add(e.getBookName() + " By : " + e.getAuthor());
		}
		Main.getInstance().setBook(daoBook.getALL());
		adminBookDropdownOut.getItems().clear();
		for (Book e : Main.getInstance().getBook()) {
			if (e.getQuantityOnHand() == 0) {
				adminBookDropdownOut.getItems().add(e.getBookName() + " By : " + e.getAuthor());
			} else {

			}
		}
		tabPane.getSelectionModel().select(0);
		// get all Genres
		Main.getInstance().setGenre(daoGenre.getALL());

		adminGenreDropdown.getItems().clear();

		for (Genre g : Main.getInstance().getGenre()) {

			adminGenreDropdown.getItems().add(g.getGenreName());
		}

	}

	@FXML
	void BackToBooks() {
		tabPane.getSelectionModel().select(1);
	}

	@FXML
	void EditGenreTab() {

	}

	@FXML
	void UserOrdersTab() {
		cancelAddress();
		closeEditPayment();
		AdminOrdersGrid.getChildren().clear();
		// -fx-grid-lines-visible: true;
		AdminOrdersGrid.setStyle("-fx-border-color: black;");
		int col = 0;
		int row = 0;
		for (Invoice r : myReports.getInvoiceByUserID(Main.getInstance().getUser().getUserID())) {
			Button invoiceButton = new Button();
			Label orderNumber = new Label();
			Label userID = new Label();
			Label lastName = new Label();
			Label firstName = new Label();
			Label creditCard = new Label();
			Label orderDate = new Label();
			invoiceButton.setText("Save Invoice");
			invoiceButton.setOnAction((event) -> {
				myReports.GenerateInvoice(r.getOrderNumber());
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("INVOICE");
				alert.setHeaderText("Invoice Saved");
				alert.setContentText("Your invoice has been saved.");
				alert.showAndWait();
			});
			orderNumber.setText(" " + String.valueOf(r.getOrderNumber()));
			userID.setText(" " + String.valueOf(r.getUserID()));
			lastName.setText(" " + r.getLastName());
			firstName.setText(" " + r.getFirstName());
			creditCard.setText(" " + String.valueOf(r.getCreditCard()));
			orderDate.setText(" " + String.valueOf(r.getOrderDate()));

			AdminOrdersGrid.add(invoiceButton, col++, row);
			AdminOrdersGrid.add(orderNumber, col++, row);
			AdminOrdersGrid.add(userID, col++, row);
			AdminOrdersGrid.add(lastName, col++, row);
			AdminOrdersGrid.add(firstName, col++, row);
			AdminOrdersGrid.add(creditCard, col++, row);
			AdminOrdersGrid.add(orderDate, col++, row);

			row++;
			col = 0;
		}
		tabPane.getSelectionModel().select(9);

	}

	@FXML
	void OpenOrdersTab() {
		cancelAddress();
		closeEditPayment();
		AdminOrdersGrid.getChildren().clear();
		int col = 0;
		int row = 0;
		for (Invoice r : myReports.getAllInvoice()) {
			Button invoiceButton = new Button();
			Label orderNumber = new Label();
			Label userID = new Label();
			Label lastName = new Label();
			Label firstName = new Label();
			Label creditCard = new Label();
			Label orderDate = new Label();
			invoiceButton.setText("Save Invoice");
			invoiceButton.setOnAction((event) -> {
				myReports.GenerateInvoice(r.getOrderNumber());
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("INVOICE");
				alert.setHeaderText("Invoice Saved");
				alert.setContentText("Your invoice has been saved.");
				alert.showAndWait();
			});
			orderNumber.setText(" " + String.valueOf(r.getOrderNumber()));
			userID.setText(" " + String.valueOf(r.getUserID()));
			lastName.setText(" " + r.getLastName());
			firstName.setText(" " + r.getFirstName());
			creditCard.setText(" " + String.valueOf(r.getCreditCard()));
			orderDate.setText(" " + String.valueOf(r.getOrderDate()));

			AdminOrdersGrid.add(invoiceButton, col++, row);
			AdminOrdersGrid.add(orderNumber, col++, row);
			AdminOrdersGrid.add(userID, col++, row);
			AdminOrdersGrid.add(lastName, col++, row);
			AdminOrdersGrid.add(firstName, col++, row);
			AdminOrdersGrid.add(creditCard, col++, row);
			AdminOrdersGrid.add(orderDate, col++, row);
			// -fx-grid-lines-visible: true;
			AdminOrdersGrid.setStyle("-fx-border-color: black;");

			row++;
			col = 0;
		}
		tabPane.getSelectionModel().select(9);

	}

	@FXML
	void CSVAllUsers() {
		myReports.getUsers();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("REPORT");
		alert.setHeaderText("Users saved");
		alert.setContentText("The Users have been saved to the CSV file.");
		alert.showAndWait();
	}

	@FXML
	void CSVInventory() {
		myReports.getInventory();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("INVENTORY");
		alert.setHeaderText("Inventory Saved");
		alert.setContentText("The inventory has been saved to the CSV file.");
		alert.showAndWait();
	}

	@FXML
	void Profits() {
		myReports.getProfits();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("PROFITS");
		alert.setHeaderText("Profits Saved");
		alert.setContentText("The profits have been saved to the CSV file.");
		alert.showAndWait();

	}

	@FXML
	void AboutTab() {
		cancelAddress();
		closeEditPayment();
		tabPane.getSelectionModel().select(10);
	}

	@FXML
	void EditPublicationTab() {
		tabPane.getSelectionModel().select(4);
		if (adminPublicationDropdown2.getSelectionModel().getSelectedIndex() >= 0) {
			adminPublicationDropdown.getSelectionModel()
					.select(adminPublicationDropdown2.getSelectionModel().getSelectedIndex());
		}
	}

	@FXML
	void CreateBook() {
		try {
			// Book
			myBook.setISBN((txtISBN.getText()));
			myBook.setBookName(txtBookName.getText());
			myBook.setAuthor(txtAuthor.getText());
			myBook.setRetailPrice(Double.parseDouble(txtRetailPrice.getText()));
			myBook.setBinding((txtBinding.getText()));
			myBook.setQuantityOnHand(Integer.parseInt(txtQuantityOnHand.getText()));
			myBook.setWholesale((Double.parseDouble(txtWholesale.getText())));
			myBook.setPublicationDate((LocalDate.parse(txtPublicationDate.getText())));

			if (adminPublicationDropdown2.getSelectionModel().getSelectedIndex() >= 0) {
				myBook.setPublicationID(Main.getInstance().getPublishers()
						.get(adminPublicationDropdown2.getSelectionModel().getSelectedIndex()).getPublicationID());

				if (adminGenreDropdown.getSelectionModel().getSelectedIndex() >= 0) {
					myBook.setGenreID(Main.getInstance().getGenre()
							.get(adminGenreDropdown.getSelectionModel().getSelectedIndex()).getGenreID());
					daoBook.create(myBook);

					ClearAdminFields();
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("CREATE");
					alert.setHeaderText("Book Create");
					alert.setContentText("The Book has been Created.");
					alert.showAndWait();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText("Form not complete");
					alert.setContentText("Ooops, there was an error! Please select a Genre");
					alert.showAndWait();

				}
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Form not complete");
				alert.setContentText("Ooops, there was an error! Please select a Publisher");
				alert.showAndWait();
			}
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Form not complete");
			alert.setContentText("Ooops, there was an error! Please fill out the whole form");
			alert.showAndWait();
		}
		EditBookTab();
	}

	@FXML
	void CreatePublisher() {
		// Address
		myAddress.setAddressLineOne(txtAdminPublisherAddress.getText());
		myAddress.setCity(txtAdminPublisherCity.getText());
		myAddress.setState(txtAdminPublisherState.getText());
		myAddress.setZip(txtAdminPublisherZip.getText());
		myAddress.setCountry(txtAdminPublisherCountry.getText());
		myAddress.setAddressFirstName(txtAdminPublisher.getText());
		myAddress.setActive("T");

		int addressID = daoAddress.createForBook(myAddress);

		// Phone
		myPhone.setPhoneNumber(txtAdminPublisherPhone.getText());
		myPhone.setPhoneType(txtAdminPublisherPhoneType.getText());
		int phoneID = daoPhone.createForBook(myPhone);

		// Publication\
		myPublication.setPublisherName(txtAdminPublisher.getText());
		myPublication.setContactName(txtAdminPublisherContact.getText());
		myPublication.setAddressID(addressID);
		myPublication.setPhoneID(phoneID);
		daoPublication.create(myPublication);

		ClearAdminPublicationFields();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("CREATE");
		alert.setHeaderText("Publisher Create");
		alert.setContentText("The Publisher has been Created.");
		alert.showAndWait();
		EditBookTab();
	}

	@FXML
	void UpdatePublisher() {

		// Address
		myAddress.setAddressLineOne(txtAdminPublisherAddress.getText());
		myAddress.setCity(txtAdminPublisherCity.getText());
		myAddress.setState(txtAdminPublisherState.getText());
		myAddress.setZip(txtAdminPublisherZip.getText());
		myAddress.setCountry(txtAdminPublisherCountry.getText());
		myAddress.setAddressFirstName(txtAdminPublisher.getText());
		myAddress.setAddressID(myPublication.getAddressID());

		daoAddress.update(myAddress);

		// Phone
		myPhone.setPhoneNumber(txtAdminPublisherPhone.getText());
		myPhone.setPhoneType(txtAdminPublisherPhoneType.getText());
		myPhone.setPhoneID(myPublication.getPhoneID());

		daoPhone.update(myPhone);

		// Publication
		myPublication.setPublisherName(txtAdminPublisher.getText());
		myPublication.setContactName(txtAdminPublisherContact.getText());

		daoPublication.update(myPublication);
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("UPDATE");
		alert.setHeaderText("Publisher Update");
		alert.setContentText("The Publisher has been updated.");
		alert.showAndWait();
<<<<<<< HEAD

=======
>>>>>>> 027a5671bac1319ddae72540b5d06ccc19c662b9
		EditBookTab();
	}

	@FXML
	void DeletePublisher() {
		if (adminPublicationDropdown.getSelectionModel().getSelectedIndex() != -1) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText("Confim deletion");
			alert.setContentText("Are you sure you want to delete the selected Publisher?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				daoPublication.delete(myPublication);
				AdminLoadPublication();
			} else {
				// ... user chose CANCEL or closed the dialog
			}

		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Form not complete");
			alert.setContentText("Ooops, there was an error! Please select a Publisher to delete");

			alert.showAndWait();
		}
	}

	void GetBook(int pBookID) {
		// This sets the book
		myBook = daoBook.get(pBookID);

		// This creates the publication class with the correct book
		myBook.myPublication = myBook.daoPublication.get(myBook.getPublicationID());

		// This is for the contact phone number
		Phone myPhone = new Phone();
		PhoneDao daoPhone = new PhoneDao();

		myPhone = daoPhone.get(myBook.myPublication.getPhoneID());
		lblPublisherNumber.setText(String.valueOf(myPhone.getPhoneNumber()));
		// This sets the address
		myAddress = daoAddress.get(myBook.myPublication.getAddressID());

		lblAddressLine1.setText(myAddress.getAddressLineOne());
		lblCity.setText(myAddress.getCity());
		lblState.setText(myAddress.getState());
		lblZip.setText(myAddress.getZip());
		lblCountry.setText(myAddress.getCountry());

		// this changes the index of the tab pane to the BookInfo
		tabPane.getSelectionModel().select(2);

		lblISBN.setText(String.valueOf(myBook.getISBN()));
		lblBookName.setText(myBook.getBookName());
		lblAuthor.setText(myBook.getAuthor());
		DecimalFormat df2 = new DecimalFormat(".00");
		lblRetailPrice.setText(df2.format(myBook.getRetailPrice()));
		lblBinding.setText(myBook.getBinding());
		lblQuantity.setText(String.valueOf(myBook.getQuantityOnHand()));
		lblGenre.setText(String.valueOf(myBook.getGenreName()));

		Image img;
		try {
			img = new Image("images/" + myBook.getBookID() + ".jpg");
		} catch (Exception ee) {
			img = new Image("images/0.jpg");
		}

		ImageView bookImage = new ImageView();
		bookImage.setFitHeight(225);
		bookImage.setFitWidth(200);
		bookImage.setImage(img);
		BookInfoGrid.add(bookImage, 0, 9);
		lblPublisherName.setText(myBook.myPublication.getPublisherName());
		lblPublisherContactName.setText(myBook.myPublication.getContactName());
		lblCopyrightDate.setText(String.valueOf(myBook.getPublicationDate()));

		btnAddToCart.setOnAction((event) -> {
			CartItem sc = new CartItem();
			List<CartItem> reference = new ArrayList<CartItem>();
			reference = daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID());
			if (reference.size() == 0) {
				sc.setUserId(Main.getInstance().getUser().getUserID());
				sc.setBookID(myBook.getBookID());
				sc.setQuantity(1);
				daoUser.addToShoppingCart(sc);
				cartQ = daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID()).size();
				btnShoppingCart.setText("Shopping Cart (" + cartQ + ")");
			} else if (reference.size() != 0) {
				int flag = 0;
				for (CartItem ref : reference) {
					if (ref.getBookID() == myBook.getBookID()) {
						flag = flag + 1;
						if (ref.getQuantity() == myBook.getQuantityOnHand()) {
							flag = flag + 1;
						}
					} else if (ref.getBookID() != myBook.getBookID()) {
						flag = flag + 0;
					}
				}
				if (flag == 1) {
					Main.getInstance().getUser().getShoppingCart().removeAll(reference);

					// This code will add the shopping carts in the database back to the users main
					// instance.

					sc.setQuantity(
							daoUser.getShoppingCartItem(Main.getInstance().getUser().getUserID(), myBook.getBookID())
									.getQuantity() + 1);
					sc.setUserId(Main.getInstance().getUser().getUserID());
					sc.setBookID(myBook.getBookID());

					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Add Book Succesfully");
					alert.setContentText(
							"Since you already have this book in your cart, the quantity has increased by one.");
					alert.showAndWait();

					daoUser.updateUserCartItem(sc);
					Main.getInstance().getUser().getShoppingCart()
							.addAll(daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID()));
					clearGridPanes();
				}
				if (flag == 0) {
					sc.setUserId(Main.getInstance().getUser().getUserID());
					sc.setBookID(myBook.getBookID());
					sc.setQuantity(1);
					daoUser.addToShoppingCart(sc);
					cartQ = daoUser.getShoppingCartItemsByUserId(Main.getInstance().getUser().getUserID()).size();
					btnShoppingCart.setText("Shopping Cart (" + cartQ + ")");
				}
				if (flag == 2) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Book addition exceeds current stock");
					alert.setContentText(
							"You have exceeded the stock for this item. Try again when there are more items in stock.");
					alert.showAndWait();
				}

			}
		});
	}

	void getAddress(int pAddressID) {
		// This sets the address
		myAddress = daoAddress.get(pAddressID);
		newAddressTab();
		btnDeleteAddress.setVisible(true);
		btnCreateNewAddress.setVisible(false);
		btnUpdateAddress.setVisible(true);

		addressFirstName.setText(myAddress.getAddressFirstName());
		addressLastName.setText(myAddress.getAddressLastName());
		addressLineOne.setText(myAddress.getAddressLineOne());
		addressLineTwo.setText(myAddress.getAddressLineTwo());
		city.setText(myAddress.getCity());
		state.setText(myAddress.getState());
		zip.setText(myAddress.getZip());
		country.setText(myAddress.getCountry());
	}

	@FXML
	private void createNewAddress(ActionEvent event) {
		Address address = new Address();
		int id = Main.getInstance().getUser().getUserID();
		address.setAddressFirstName(addressFirstName.getText());
		address.setAddressLastName(addressLastName.getText());
		address.setAddressLineOne(addressLineOne.getText());
		address.setAddressLineTwo(addressLineTwo.getText());
		address.setCity(city.getText());
		address.setState(state.getText());
		address.setZip(zip.getText());
		address.setCountry(country.getText());
		address.setActive("T");
		address.setUserID(id);
		daoAddress.create(address);
		newAddressTab();

		clearGridPanes();
		generateAddresses();
		generateAddressesAndPaymentsForCheckout();
		startTab();
	}

	@FXML
	private void cancelAddress() {
		rightTabPane.getSelectionModel().select(0);
	}

	void getPayment(int pUserId) {
		// This sets the address
		myPayment = daoPayment.get(pUserId);
		myAddress = daoAddress.get(myPayment.getAddressID());
		newPaymentTab();
		btnDeletePayment.setVisible(true);
		btnCreatePaymentPane.setVisible(false);
		btnUpdatePaymentPane.setVisible(true);

		cardNumber.setText(myPayment.getCardNumber());
		cardType.setText(myPayment.getCardType());

		Date date = myPayment.getExpDate();
		LocalDate input = date.toLocalDate();
		int day = input.getDayOfMonth();
		int year = input.getYear();
		int month = input.getMonthValue();
		expDate.setValue(LocalDate.of(year, month, day));

		cvv.setText(myPayment.getCvv());
		paymentFirstName.setText(myAddress.getAddressFirstName());
		paymentLastName.setText(myAddress.getAddressLastName());
		addressLineOnePay.setText(myAddress.getAddressLineOne());
		addressLineTwoPay.setText(myAddress.getAddressLineTwo());
		cityPay.setText(myAddress.getCity());
		// TODO: state
		zipPay.setText(myAddress.getZip());
		countryPay.setText(myAddress.getCountry());
		addressLineOnePay.setText(myAddress.getAddressLineOne());
		addressLineTwoPay.setText(myAddress.getAddressLineTwo());
		cityPay.setText(myAddress.getCity());
		statePay.setText(myAddress.getState());
		zipPay.setText(myAddress.getZip());
		countryPay.setText(myAddress.getCountry());
	}

	@FXML
	void createPayment(ActionEvent event) throws IOException {
		PaymentDao pDao = new PaymentDao();
		AddressDao aDao = new AddressDao();
		User user = Main.getInstance().getUser();

		myPayment.setCardNumber(cardNumber.getText());
		myPayment.setCardType(cardType.getText());
		Date date = Date.valueOf(expDate.getValue());
		myPayment.setExpDate(date);
		myPayment.setCvv(cvv.getText());
		myPayment.setLastFour(cardNumber.getText().substring(12));
		myPayment.setActive("T");
		myPayment.setUserID(user.getUserID());

		myAddress.setAddressLastName(paymentLastName.getText());
		myAddress.setAddressFirstName(paymentFirstName.getText());
		myAddress.setAddressLineOne(addressLineOnePay.getText());
		myAddress.setAddressLineTwo(addressLineTwoPay.getText());
		myAddress.setCity(cityPay.getText());
		// Will need to fix this states option later
		myAddress.setState(statePay.getText());
		myAddress.setZip(zipPay.getText());
		myAddress.setCountry(countryPay.getText());
		myAddress.setUserID(user.getUserID());

		// the two SQL queries that will be executed to create a payment
		aDao.create(myAddress);
		pDao.create(myPayment);

		clearGridPanes();
		generatePayments();
		generateAddressesAndPaymentsForCheckout();
		startTab();

	}

	@FXML
	private void updatePayment(ActionEvent event) {
		PaymentDao pDao = new PaymentDao();
		AddressDao aDao = new AddressDao();
		User user = Main.getInstance().getUser();

		myPayment.setCardNumber(cardNumber.getText());
		myPayment.setCardType(cardType.getText());
		Date date = Date.valueOf(expDate.getValue());
		myPayment.setExpDate(date);
		myPayment.setCvv(cvv.getText());
		myPayment.setLastFour(cardNumber.getText().substring(12));
		myPayment.setActive("T");
		myPayment.setUserID(user.getUserID());

		myAddress.setAddressLastName(paymentLastName.getText());
		myAddress.setAddressFirstName(paymentFirstName.getText());
		myAddress.setAddressLineOne(addressLineOnePay.getText());
		myAddress.setAddressLineTwo(addressLineTwoPay.getText());
		myAddress.setCity(cityPay.getText());
		// Will need to fix this states option later
		myAddress.setState(statePay.getText());
		myAddress.setZip(zipPay.getText());
		myAddress.setCountry(countryPay.getText());
		myAddress.setUserID(user.getUserID());

		// the two SQL queries that will be executed to create a payment
		aDao.update(myAddress);
		pDao.update(myPayment);

		clearTextFieldsForPaymentPane();
		closeEditPayment();
		clearGridPanes();
		generatePayments();
		paymentsTab();
	}

	@FXML
	private void deletePayment(ActionEvent event) {
		PaymentDao pDao = new PaymentDao();
		pDao.delete(myPayment);
		clearTextFieldsForPaymentPane();
		clearGridPanes();
		generatePayments();
		paymentsTab();
		closeEditPayment();
	}

	@FXML
	private void closeEditPayment() {
		rightTabPane.getSelectionModel().select(0);
	}

	private void clearTextFieldsForPaymentPane() {
		cardNumber.setText("");
		cardType.setText("");
		expDate.setValue(null);
		cvv.setText("");
		paymentFirstName.setText("");
		paymentLastName.setText("");
		addressLineOnePay.setText("");
		addressLineTwoPay.setText("");
		cityPay.setText("");
		statePay.setText("");
		zipPay.setText("");
		countryPay.setText("");
	}

	@FXML
	void AdminLoadBook() {

		if (adminBookDropdown.getSelectionModel().getSelectedIndex() >= 0) {
			myBook = daoBook.get(Main.getInstance().getBook()
					.get(adminBookDropdown.getSelectionModel().getSelectedIndex()).getBookID());
		}

		myBook.myPublication = myBook.daoPublication.get(myBook.getPublicationID());
		myPhone = daoPhone.get(myBook.myPublication.getPhoneID());
		myAddress = daoAddress.get(myBook.myPublication.getAddressID());

		// Book

		txtISBN.setText(String.valueOf(myBook.getISBN()));
		txtBookName.setText(myBook.getBookName());
		txtAuthor.setText(myBook.getAuthor());
		DecimalFormat df2 = new DecimalFormat(".00");
		txtRetailPrice.setText(df2.format(myBook.getRetailPrice()));
		txtBinding.setText(myBook.getBinding());
		txtQuantityOnHand.setText(String.valueOf(myBook.getQuantityOnHand()));
		txtWholesale.setText(String.valueOf(myBook.getWholesale()));
		txtPublicationDate.setText(String.valueOf(myBook.getPublicationDate()));
		int i = 0;
		int t = 0;
		for (Genre g : Main.getInstance().getGenre()) {
			if (g.getGenreID() == myBook.getGenreID()) {
				t = i;
			}
			i++;
		}
		adminGenreDropdown.getSelectionModel().select(t);
		i = 0;
		t = 0;
		for (Publication p : Main.getInstance().getPublishers()) {
			if (p.getPublicationID() == myBook.getPublicationID()) {
				t = i;
			}
			i++;
		}
		adminPublicationDropdown2.getSelectionModel().select(t);

		// Publication may not be used
		txtAdminPublisher.setText(myBook.myPublication.getPublisherName());
		txtAdminPublisherContact.setText(myBook.myPublication.getContactName());
		txtAdminPublisherPhone.setText(myPhone.getPhoneNumber());
		txtAdminPublisherPhoneType.setText(myPhone.getPhoneType());
		txtAdminPublisherAddress.setText(myAddress.getAddressLineOne());
		txtAdminPublisherCity.setText(myAddress.getCity());
		txtAdminPublisherState.setText(myAddress.getState());
		txtAdminPublisherZip.setText(myAddress.getZip());
		txtAdminPublisherCountry.setText(myAddress.getCountry());
	}

	@FXML
	void AdminDeleteBook() {
		if (adminBookDropdown.getSelectionModel().getSelectedIndex() >= 0) {
			myBook = daoBook.get(Main.getInstance().getBook()
					.get(adminBookDropdown.getSelectionModel().getSelectedIndex()).getBookID());
			daoBook.delete(myBook);
			ClearAdminFields();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText("Confim deletion");
			alert.setContentText("Are you sure you want to delete the selected Book?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {

				myBook = daoBook.get(Main.getInstance().getBook()
						.get(adminBookDropdown.getSelectionModel().getSelectedIndex()).getBookID());
				daoBook.delete(myBook);
			} else {
				// ... user chose CANCEL or closed the dialog
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Form not complete");
			alert.setContentText("Ooops, there was an error! Please select a Book");
			alert.showAndWait();
		}
		EditBookTab();
	}

	@FXML
	void ClearAdminFields() {
		// Book
		txtISBN.clear();
		txtBookName.clear();
		txtAuthor.clear();
		txtRetailPrice.clear();
		txtBinding.clear();
		txtQuantityOnHand.clear();
		txtWholesale.clear();
		txtPublicationDate.clear();

	}

	@FXML
	void ClearAdminPublicationFields() {
		// Publication
		txtAdminPublisher.clear();
		txtAdminPublisherContact.clear();
		txtAdminPublisherPhone.clear();
		txtAdminPublisherPhoneType.clear();
		txtAdminPublisherAddress.clear();
		txtAdminPublisherCity.clear();
		txtAdminPublisherState.clear();
		txtAdminPublisherZip.clear();
		txtAdminPublisherCountry.clear();
	}

	@FXML
	void AdminUpdateBook() {
		if (adminBookDropdown.getSelectionModel().getSelectedIndex() >= 0) {
			myBook = daoBook.get(Main.getInstance().getBook()
					.get(adminBookDropdown.getSelectionModel().getSelectedIndex()).getBookID());
		}
		myBook.setISBN(txtISBN.getText());
		myBook.setBookName(txtBookName.getText());
		myBook.setAuthor(txtAuthor.getText());
		myBook.setRetailPrice(Double.parseDouble(txtRetailPrice.getText()));
		myBook.setBinding(txtBinding.getText());
		myBook.setQuantityOnHand(Integer.parseInt(txtQuantityOnHand.getText()));
		myBook.setWholesale((Double.parseDouble(txtWholesale.getText())));
		myBook.setPublicationDate((LocalDate.parse(txtPublicationDate.getText())));

		if (adminPublicationDropdown2.getSelectionModel().getSelectedIndex() >= 0) {
			myBook.setPublicationID(Main.getInstance().getPublishers()
					.get(adminPublicationDropdown2.getSelectionModel().getSelectedIndex()).getPublicationID());

			if (adminGenreDropdown.getSelectionModel().getSelectedIndex() >= 0) {
				myBook.setGenreID(Main.getInstance().getGenre()
						.get(adminGenreDropdown.getSelectionModel().getSelectedIndex()).getGenreID());
				daoBook.update(myBook);
				ClearAdminFields();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("UPDATE");
				alert.setHeaderText("Book Update");
				alert.setContentText("The Book has been updated.");
				alert.showAndWait();
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Form not complete");
				alert.setContentText("Ooops, there was an error! Please select a Genre");
				alert.showAndWait();

			}

		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Form not complete");
			alert.setContentText("Ooops, there was an error! Please select a Publisher");
			alert.showAndWait();
		}
		EditBookTab();
	}

	@FXML
	void EditEmail() {

	}

	@FXML
	void EditPassword() {

	}
	/*
	 * }
	 * 
	 * /*
	 * 
	 * @FXML void BackToBooks() { tabPane.getSelectionModel().select(1); } /*
	 * 
	 * @FXML void runUpdateBook() {
	 * myBook.setISBN(Integer.parseInt(txtISBN.getText()));
	 * myBook.setBookName(txtBookName.getText());
	 * myBook.setAuthor(txtAuthor.getText());
	 * myBook.setPrice(Double.parseDouble(txtPrice.getText()));
	 * daoBook.update(myBook); }
	 * 
	 * @FXML void runDeleteBook() {
	 * myBook.setISBN(Integer.parseInt(txtISBN.getText())); daoBook.delete(myBook);
	 * runClear(); }
	 * 
	 * @FXML void runClear() { txtISBN.clear(); txtBookName.clear();
	 * txtAuthor.clear(); txtPrice.clear(); }
	 * 
	 * @FXML void runKeyPressed(KeyEvent e) { if (e.getCode() == KeyCode.DELETE) {
	 * e.consume(); //runClearOutput(); } }
	 */

	// -------------------Actions fired from the MainMenu.fxml-------------------
	/**
	 * This method is for creating an additional window for an FXML "stage"
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void newWindow(ActionEvent event) throws Exception {
		// this code creates a button object, which contains a method that can grab
		// the id of the button pressed
		btn = (Button) event.getSource();
		String id = btn.getId();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(determinePath(id)));
		Parent root = (Parent) fxmlLoader.load();
		Stage stage = new Stage();
		// this modal disables the main stage before this stage is open
		stage.initModality(Modality.APPLICATION_MODAL);
		// this code is to set the scene
		stage.setScene(new Scene(root));
		stage.show();
	}

	/**
	 * This method is for creating a "sub" stage with in the main "stage"
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void subWindow(ActionEvent event) throws Exception {
		String id;
		btn = (Button) event.getSource();
		id = btn.getId();
		AnchorPane centerPane = FXMLLoader.load(getClass().getResource(determinePath(id)));
		centerRootPane.getChildren().setAll(centerPane);

		if (id.equals("btnEditAddress")) {
			id = "createAddress";
			title.setText("Shipping Addresses");
		} else if (id.equals("btnEditPayment")) {
			id = "createPayment";
			title.setText("Payment Methods");
		} else if (id.equals("btnEditPhone")) {
			id = "createPhone";
			title.setText("Phone Numbers");
		}

		AnchorPane rightPane = FXMLLoader.load(getClass().getResource(determinePath(id)));
		rightRootPane.getChildren().setAll(rightPane);
	}

	/**
	 * This method takes a parameter of the string id from the FXML button
	 * attributes and then changes it into a the proper build path
	 * 
	 * @param pid
	 * @return
	 */
	public String determinePath(String pid) {
		// this conditional sets the correct path for the FXML file
		// depending on the button pressed
		String fxmlPath = null;

		if (pid.equals("btnSignInMain"))
			fxmlPath = "../view/SignIn.fxml";
		else if (pid.equals("btnNewUser"))
			fxmlPath = "../view/NewUser.fxml";

		else if (pid.equals("btnBestSellers"))
			fxmlPath = "../view/BestSellers.fxml";

		else if (pid.equals("btnEditAddress"))
			fxmlPath = "../view/AddressCardHolder.fxml";
		else if (pid.equals("createAddress"))
			fxmlPath = "../view/EditAddress.fxml";

		else if (pid.equals("btnEditPayment"))
			fxmlPath = "../view/PaymentCardHolder.fxml";
		else if (pid.equals("createPayment"))
			fxmlPath = "../view/EditPayment.fxml";

		else if (pid.equals("btnEditPhone"))
			fxmlPath = "../view/PhoneCardHolder.fxml";
		else if (pid.equals("createPhone"))
			fxmlPath = "../view/EditPhone.fxml";

		return fxmlPath;
	}

	// -------------------------Actions fired from the
	// SignIn.fxml-------------------
	/**
	 * This method loads the "NewUser" creation page from the sign in page by
	 * setting the contents in the sing in page to the contents of the "NewUser"
	 * page.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void loadNewUser(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("../view/NewUser.fxml"));
		centerRootPane.getChildren().setAll(pane);
	}

	/**
	 * This is the signIn action event.
	 * 
	 * @param event
	 */
	@FXML
	private void signInActionEvent(ActionEvent event) {
		UserDao ud = new UserDao();
		Main.getInstance().setUser(ud.get(emailSignIn.getText(), passwordSignIn.getText()));
		User user = Main.getInstance().getUser();

		if (user.getUserID() == 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("The username or password does not match any accounts in the database.");
			alert.showAndWait();
		} else if (user != null) {
			// this code simply closes the log in screen when it is done
			Node source = (Node) event.getSource();
			Stage stage = (Stage) source.getScene().getWindow();
			stage.close();
		}
	}

	/**
	 * This event brings the user to a new window to create an account.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void newUserActionEvent(ActionEvent event) throws IOException {

		AnchorPane pane = FXMLLoader.load(getClass().getResource("NewUser.fxml"));
		centerRootPane.getChildren().setAll(pane);
	}

	// -----------------------Actions fired from the EditAddress.fxml---------------
	public void loadEditAddress(ActionEvent event) throws Exception {
		MainController mc = new MainController();
		mc.subWindow(event);
	}

	/**
	 * I've created an enumeration for all of these state values but I'm not sure
	 * how to put it into the comboBox, so I will leave them stored in this list
	 * instead for now.
	 */
	ObservableList<String> states = FXCollections.observableArrayList("Alaska", "Arizona", "Arkansas", "California",
			"Colorado", "CT", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
			"Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota",
			"Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico",
			"New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island",
			"South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington",
			"West Virginia", "Wisconsin", "Wyoming");

	// @FXML
	// private ComboBox<String> statesComboBox;
	// /**
	// * Initializes the comboBox with
	// * all of the list options defined above.
	// */
	// @FXML
	// private void initialize() {
	// statesComboBox.setValue("Alabama");
	// statesComboBox.setItems(states);
	// }
	/**
	 * This method sends all of the address info to the model and then creates an
	 * address in the database.
	 * 
	 * @param event
	 */
	@FXML
	private void createAddress(ActionEvent event) {
		Address address = new Address();
		AddressDao aDao = new AddressDao();
		User user = Main.getInstance().getUser();

		address.setAddressLineOne(addressLineOne.getText());
		address.setAddressLineTwo(addressLineTwo.getText());
		address.setCity(city.getText());

		// will need to figure out how to code how to get the index that is
		// selected in the combo box
		address.setState(states.get(5));
		address.setZip(zip.getText());
		address.setCountry(country.getText());
		address.setAddressLastName(addressLastName.getText());
		address.setAddressFirstName(addressFirstName.getText());
		address.setUserID(user.getUserID());
		aDao.create(address);
		clearEditAddressTextFields();
	}

	@FXML
	private void loadAddress(ActionEvent event) {

		validateAddress();

		AddressDao aDao = new AddressDao();
		Main.getInstance().setAddress(aDao.get(Integer.parseInt(txtFieldAddressID.getText())));
		Address address = Main.getInstance().getAddress();

		addressFirstName.setText(address.getAddressFirstName());
		addressLastName.setText(address.getAddressLastName());
		addressLineOne.setText(address.getAddressLineOne());
		addressLineTwo.setText(address.getAddressLineTwo());
		city.setText(address.getCity());
		// states.set(2, String);
		zip.setText(address.getZip());
		country.setText(address.getCountry());

		btnUpdateAddress.setDisable(false);
	}

	@FXML
	private void deleteAddress(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Deletion of Records");
		alert.setContentText("Are you sure you want to delete these records?");
		alert.showAndWait();
		ButtonType r = alert.getResult();

		if (r != ButtonType.CANCEL) {
			AddressDao aDao = new AddressDao();
			aDao.delete(myAddress);

			clearGridPanes();
			generateAddresses();
			addressTab();
			startTab();
		} else {
		}
	}

	@FXML
	private void updateAddress(ActionEvent event) {
		myAddress.setAddressLineOne(addressLineOne.getText());
		myAddress.setAddressLineTwo(addressLineTwo.getText());
		myAddress.setCity(city.getText());

		// will need to figure out how to code how to get the index that is
		// selected in the combo box
		myAddress.setState(states.get(5));
		myAddress.setZip(zip.getText());
		myAddress.setCountry(country.getText());
		myAddress.setAddressLastName(addressLastName.getText());
		myAddress.setAddressFirstName(addressFirstName.getText());
		AddressDao aDao = new AddressDao();
		aDao.update(myAddress);

		clearGridPanes();
		generateAddresses();
		addressTab();
		startTab();

	}

	/**
	 * Checks to see if the address exists in the database
	 */
	private void validateAddress() {
		AddressDao aDao = new AddressDao();
		Main.getInstance().setAddress(aDao.get(Integer.parseInt(txtFieldAddressID.getText())));
		Address address = Main.getInstance().getAddress();

		int addressID = address.getAddressID();
		if (addressID == 0) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Error");
			alert.setContentText("There are no addresses in the database that match this ID.");
			alert.showAndWait();
		}
	}

	/**
	 * Helps checking to see if the addressID text field has any input. The method
	 * that calls this method will throw a javaFX alert.
	 * 
	 * @param pFlag
	 * @return
	 */

	private void clearEditAddressTextFields() {
		addressFirstName.setText("");
		addressLastName.setText("");
		addressLineOne.setText("");
		addressLineTwo.setText("");
		city.setText("");
		// states.set(2, String);
		zip.setText("");
		country.setText("");
	}

	private void clearGridPanes() {
		addressGrid.getChildren().clear();
		paymentGrid.getChildren().clear();
		shoppingCartGrid.getChildren().clear();
		checkoutAddressGrid.getChildren().clear();
		checkoutPaymentGrid.getChildren().clear();
		orderDetailGrid.getChildren().clear();
	}

	public void reload() {
		initialize();
	}

	public List<Address> filteredAddresses() {
		// this block of code is for checking to see if there are any addresses
		// that are in the shipping category as well as related to a payment.
		// it sorts the shipping addresses from the payment related ones and keeps
		// the shipping addresses only.
		List<Address> addresses = new ArrayList<Address>();
		addresses = daoAddress.getAllbyID(Main.getInstance().getUser().getUserID());
		List<Integer> addressesToRemove = new ArrayList<Integer>();
		for (Payment p : daoPayment.getAllbyID(Main.getInstance().getUser().getUserID())) {
			for (Address a : addresses) {
				if (p.getAddressID() == a.getAddressID()) {
					addressesToRemove.add(addresses.indexOf(a));
				}
			}
		}
		List<Address> revisedAddresses = new ArrayList<Address>();
		for (Address a : addresses) {
			int n = 0;
			for (Integer r : addressesToRemove) {
				if (addresses.indexOf(a) == r) {
					n++;
				}
			}
			if (n == 0) {
				revisedAddresses.add(a);
			}
		}
		// end of a ridiculous amount of code to sort a simple address into
		// the proper category - shipping addresses only. But it works!!!
		return revisedAddresses;
	}
}
