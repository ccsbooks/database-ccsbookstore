package controller;

import java.io.IOException;
import java.sql.Date;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.Address;
import model.AddressDao;
import model.Payment;
import model.PaymentDao;
import model.User;

public class PaymentController {

	private MainController main;

	public void injectMainController(MainController mainController) {
		this.main = mainController;
	}

	@FXML
	private TextField cardNumber;
	@FXML
	private TextField cardType;
	@FXML
	private DatePicker expDate;
	@FXML
	private TextField cvv;
	@FXML
	private TextField cardActive;
	@FXML
	private TextField addressLineOnePay;
	@FXML
	private TextField addressLineTwoPay;
	@FXML
	private TextField cityPay;
	@FXML
	private ComboBox statesComboBox;
	@FXML
	private TextField zipPay;
	@FXML
	private TextField countryPay;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField txtFieldPaymentID;
	@FXML
	private Button btnCreatePaymentPane;
	@FXML
	private Button btnUpdatePaymentPane;
	@FXML
	private Button btnDeletePayment;
	@FXML
	private Button btnCancelPane;
	@FXML
	private Label lblPayment;
	@FXML
	private AnchorPane editPayment;

	Payment payment = new Payment();
	Address address = new Address();

	@FXML
	private void initialize() {
		
		
		editPayment.setVisible(true);
		btnUpdatePaymentPane.setVisible(false);
		btnCreatePaymentPane.setVisible(false);
		int id = Main.getInstance().getPayment().getPaymentID();
		if (id == 0) {
			lblPayment.setText("Create New Payment Method");
			btnCreatePaymentPane.setVisible(true);
		} else if (id != 0) {
			btnUpdatePaymentPane.setVisible(true);
		}

	}

	/**
	 * This method sends all of the address info to the model and then creates an
	 * address in the database.
	 * 
	 * @param event
	 * @throws IOException 
	 */
	@FXML
	private void createPayment(ActionEvent event) throws IOException {
		PaymentDao pDao = new PaymentDao();
		AddressDao aDao = new AddressDao();
		User user = Main.getInstance().getUser();

//		payment.setCardNumber(cardNumber.getText());
//		payment.setCardType(cardType.getText());
//		Date date = Date.valueOf(expDate.getValue());
//		payment.setExpDate(date);
//		payment.setCvv(cvv.getText());
//		payment.setActive("T");
//		payment.setUserID(user.getUserID());
//
//		address.setAddressLastName(lastName.getText());
//		address.setAddressFirstName(firstName.getText());
//		address.setAddressLineOne(addressLineOne.getText());
//		address.setAddressLineTwo(addressLineTwo.getText());
//		address.setCity(city.getText());
//		// Will need to fix this states option later
//		address.setState("AR");
//		address.setZip(zip.getText());
//		address.setCountry(country.getText());
//		address.setUserID(user.getUserID());

		payment.setCardNumber("35343425242");
		payment.setCardType("Visa");
		Date date = Date.valueOf(expDate.getValue());
		payment.setExpDate(date);
		payment.setCvv("322");
		payment.setActive("T");
		payment.setUserID(user.getUserID());

		address.setAddressLastName("Test");
		address.setAddressFirstName("Test");
		address.setAddressLineOne("Test");
		address.setAddressLineTwo("Test");
		address.setCity("Test");
		// Will need to fix this states option later
		address.setState("AR");
		address.setZip("33456");
		address.setCountry("USA");
		address.setUserID(user.getUserID());
		
		// the two SQL queries that will be executed to create a payment
		aDao.create(address);
		pDao.create(payment);

		clearTextFields();
		closeEditPayment(event);
	}

	@FXML
	private void loadPayment(ActionEvent event) {

		PaymentDao pDao = new PaymentDao();
		Main.getInstance().setPayment(pDao.get(Integer.parseInt(txtFieldPaymentID.getText())));
		Payment payment = Main.getInstance().getPayment();

		cardNumber.setText(payment.getCardNumber());
		cardType.setText(payment.getCardType());
		expDate.setValue(payment.getExpDate().toLocalDate());
		cvv.setText(payment.getCvv());
		cardType.setText(payment.getCardType());
		cardActive.setText(payment.getActive());

		AddressDao aDao = new AddressDao();
		Main.getInstance().setAddress(aDao.get(payment.getAddressID()));
		Address address = Main.getInstance().getAddress();

		addressLineOnePay.setText(address.getAddressLineOne());
		addressLineTwoPay.setText(address.getAddressLineTwo());
		cityPay.setText(address.getCity());
		zipPay.setText(address.getZip());
		countryPay.setText(address.getCountry());
		firstName.setText(address.getAddressFirstName());
		lastName.setText(address.getAddressLastName());

	}

	@FXML
	private void updatePayment(ActionEvent event) {

	}

	@FXML
	private void deletePayment(ActionEvent event) {

		PaymentDao pDao = new PaymentDao();
		Payment payment = new Payment();
		payment.setPaymentID(Integer.parseInt(txtFieldPaymentID.getText()));
		pDao.delete(payment);
		clearTextFields();
	}

	@FXML
	private void closeEditPayment(ActionEvent event) {
		editPayment.setVisible(false);
	}

	private void clearTextFields() {
		cardNumber.setText("");
		cardType.setText("");
		expDate.setValue(null);
		cvv.setText("");
		firstName.setText("");
		lastName.setText("");
		addressLineOnePay.setText("");
		addressLineTwoPay.setText("");
		cityPay.setText("");
		// states.set(2, String);
		zipPay.setText("");
		countryPay.setText("");
	}
}
