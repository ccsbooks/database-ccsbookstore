package reports;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javafx.util.converter.LocalDateTimeStringConverter;
import model.Book;
import model.BookDao;
import model.Genre;
import model.Invoice;
import model.User;
import model.UserDao;

public class Report {
	Properties appProps = new Properties();

	public Report() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Invoice> getAllInvoice() {
		List<Invoice> allInvoice = new ArrayList<>();

		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetAllInvoice()}");
			ResultSet rsInvoice = stmt.executeQuery();
			while (rsInvoice.next()) {
				Invoice myInvoice = new Invoice();
				int n = 0;
				myInvoice.setOrderNumber(rsInvoice.getInt("PurchaseID"));
				myInvoice.setUserID(rsInvoice.getInt("UserID"));
				myInvoice.setLastName(rsInvoice.getString("LastName"));
				myInvoice.setFirstName(rsInvoice.getString("FirstName"));
				myInvoice.setCreditCard(rsInvoice.getInt("LastFour"));
				myInvoice.setOrderDate(LocalDate.parse(rsInvoice.getString("PurchaseDate")));

				allInvoice.add(myInvoice);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allInvoice;

	}

	public void GenerateInvoice(int pPurchaseID) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mmss");
			DecimalFormat df2 = new DecimalFormat(".00");
			String fileName = "../INVOICE_" + LocalDateTime.now().format(formatter);
			CSVGenerator myCSV = new CSVGenerator("Invoice", fileName);
			double total = 0;
			double discount = 0;
			CallableStatement stmt = cn.prepareCall("{call OrderInvoiceByPurchaseID(?)}");
			int n = 0;
			stmt.setInt(++n, pPurchaseID);
			ResultSet rs = stmt.executeQuery();
			boolean firstRun = true;
			while (rs.next()) {
				if (firstRun) {
					firstRun = false;
					myCSV.addRow("Order Number: ", String.valueOf(rs.getInt("PurchaseID")), "Date: ",
							rs.getString("PurchaseDate"));
					myCSV.addRow("");
					myCSV.addRow("Shipping Address", rs.getString("LastName") + " " + (rs.getString("FirstName")));
					myCSV.addRow("", String.valueOf(rs.getString("AddressLine1")));
					if (rs.getString("AddressLine2") != null) {
						myCSV.addRow("", rs.getString("AddressLine2"));
					}

					myCSV.addRow("", rs.getString("City"), rs.getString("State"), rs.getString("Zip"),
							rs.getString("Country"));
					myCSV.addRow("");
					myCSV.addRow("Credit cart:", "xxxx-xxxx-xxxx-" + rs.getString("LastFour"));
					myCSV.addRow("");
					myCSV.addRow("Title", "Quantity", "Price");

				}
				discount = 10;
				double price = Double.parseDouble(rs.getString("RetailPrice"));
				double quantity = Double.parseDouble(rs.getString("Quantity"));
				total += (price * quantity);
				myCSV.addRow(rs.getString("BookName"), String.valueOf(quantity), (" $" + df2.format(price)));
			}
			myCSV.addRow("");

			if (discount != 0) {

				myCSV.addRow("", "", "Total before discount:", (" $" + df2.format(total)));
				myCSV.addRow("", "", " Discount:", discount + "%");
				discount = total * .1;
				total = total - discount;
			}

			myCSV.addRow("", "", "Total: ", (" $" + df2.format((total))));
			try {
				myCSV.generateCSV();
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public List<Invoice> getInvoiceByUserID(int pUserID) {
		List<Invoice> allInvoice = new ArrayList<>();

		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetInvoiceByUserID(?)}");
			stmt.setInt(1, pUserID);
			ResultSet rsInvoice = stmt.executeQuery();
			while (rsInvoice.next()) {
				Invoice myInvoice = new Invoice();
				int n = 0;
				myInvoice.setOrderNumber(rsInvoice.getInt("PurchaseID"));
				myInvoice.setUserID(rsInvoice.getInt("UserID"));
				myInvoice.setLastName(rsInvoice.getString("LastName"));
				myInvoice.setFirstName(rsInvoice.getString("FirstName"));
				myInvoice.setCreditCard(rsInvoice.getInt("LastFour"));
				myInvoice.setOrderDate(LocalDate.parse(rsInvoice.getString("PurchaseDate")));

				allInvoice.add(myInvoice);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allInvoice;

	}

	public void getInventory() {
		List<Book> allBooks = new ArrayList<>();
		BookDao daoBook = new BookDao();
		allBooks = daoBook.getALL();
		DecimalFormat df2 = new DecimalFormat(".00");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mmss");
		String fileName = "../INVENTORY_" + LocalDateTime.now().format(formatter);
		CSVGenerator myCSV = new CSVGenerator("Inventory", fileName);
		myCSV.addRow("Book ID", "Book Name", "Quantity", "Wholesale", "Retail");
		for (Book b : allBooks) {
			myCSV.addRow(String.valueOf(b.getBookID()), b.getBookName(), String.valueOf(b.getQuantityOnHand()),
					(" $" + df2.format(b.getWholesale())), (" $" + df2.format(b.getRetailPrice())));
		}
		try {
			myCSV.generateCSV();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}

	public void getUsers() {
		List<User> allUsers = new ArrayList<>();
		UserDao daoUser = new UserDao();
		allUsers = daoUser.getALL();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mmss");
		String fileName = "../CUSTOMERS_" + LocalDateTime.now().format(formatter);
		CSVGenerator myCSV = new CSVGenerator("Customers", fileName);
		myCSV.addRow("User ID", "Last Name", "First Name", "Email", "Active");
		for (User u : allUsers) {
			myCSV.addRow(String.valueOf(u.getUserID()), u.getLastName(), u.getFirstName(), u.getEmail(), u.getActive());
		}
		try {
			myCSV.generateCSV();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}

	public void getProfits() {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mmss");
			DecimalFormat df2 = new DecimalFormat(".00");
			String fileName = "../SALES" + LocalDateTime.now().format(formatter);
			CSVGenerator myCSV = new CSVGenerator("SALES", fileName);
			CallableStatement stmt = cn.prepareCall("{call AllOrderInvoice()}");
			ResultSet rs = stmt.executeQuery();
			boolean firstRun = true;
			while (rs.next()) {
				if (firstRun) {
					firstRun = false;
					myCSV.addRow("PurchaseID", "PurchaseDate", "Quantity", "Discount", "RetailPrice", "Wholesale", "Bookid", "BookName");
				}
				myCSV.addRow(rs.getString("PurchaseID"), rs.getString("PurchaseDate"), rs.getString("Quantity"), rs.getString("Discount"), 
						rs.getString("RetailPrice"), rs.getString("wholesale"), rs.getString("bookid"), rs.getString("BookName"));
			}
			try {
				myCSV.generateCSV();
			} catch (FileNotFoundException e) {

				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}