package reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CSVGenerator {
	private String filePath;
	private StringBuilder sb = new StringBuilder();

	// private List<List<String>> grid = new ArrayList<>();
	public CSVGenerator(String pHeader, String fileName) {
		setFeild(pHeader);
		nextRow();
		filePath = fileName + ".csv";
	}

	public void addRow(List<String> row) {
		for (String feild : row) {
			setFeild(feild);
			nextCol();
		}
		nextRow();
	}

	public void addRow(String... row) {
		for (String feild : row) {
			setFeild(feild);
			nextCol();
		}
		nextRow();
	}

	public String getCsvString() {
		return sb.toString();
	}

	public void generateCSV() throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File(filePath));
		pw.write(getCsvString());
		pw.close();
	}

	public void reset(String pHeader, String fileName) {
		sb.setLength(0);
		setFeild(pHeader);
		nextRow();
		filePath = fileName + ".csv";
	}

	private void nextCol() {
		sb.append(',');
	}

	private void nextRow() {
		sb.append('\n');
	}

	private void setFeild(String value) {
		sb.append(value.replaceAll(",", ""));
	}
}
