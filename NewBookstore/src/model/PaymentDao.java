package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PaymentDao implements Dao<Payment> {
	Properties appProps = new Properties();

	public PaymentDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // this is a constructor for the AddressDao.java class

	@Override
	public void create(Payment objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call CreateUserPayment(?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getCardNumber());
			stmt.setDate(++n, objectToSave.getExpDate());
			stmt.setString(++n, objectToSave.getCvv());
			stmt.setString(++n, objectToSave.getCardType());
			stmt.setString(++n, objectToSave.getLastFour());
			stmt.setString(++n, objectToSave.getActive());
			stmt.setInt(++n, objectToSave.getUserID());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Payment> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Payment get(int pPaymentID) {
		Payment returnPayment = new Payment();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetPaymentByPaymentID(?)}");
			int n = 0;
			stmt.setInt(++n, pPaymentID);
			ResultSet rsPayment = stmt.executeQuery();

			if (rsPayment.next()) {
				returnPayment.setPaymentID(rsPayment.getInt("PaymentID"));
				returnPayment.setAddressID(rsPayment.getInt("AddressID"));
				returnPayment.setCardNumber(rsPayment.getString("CardNumber"));
				returnPayment.setExpDate(rsPayment.getDate("ExpDate"));
				returnPayment.setCvv(rsPayment.getString("CVV"));
				returnPayment.setCardType(rsPayment.getString("CardType"));
				returnPayment.setLastFour(rsPayment.getString("LastFour"));
				returnPayment.setActive(rsPayment.getString("Active"));
				returnPayment.setUserID(rsPayment.getInt("UserID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnPayment;
	}

	@Override
	public void update(Payment objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdatePaymentByPaymentID(?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getPaymentID());
			stmt.setString(++n, objectToUpdate.getCardNumber());
			stmt.setDate(++n, objectToUpdate.getExpDate());
			stmt.setString(++n, objectToUpdate.getCvv());
			stmt.setString(++n, objectToUpdate.getCardType());
			stmt.setString(++n, objectToUpdate.getLastFour());
			stmt.setString(++n, objectToUpdate.getActive());
			stmt.executeQuery();
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(Payment objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeletePaymentByPaymentID(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getPaymentID());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public List<Payment> getAllbyID(int pUserID) {
		List<Payment> allUserPayments = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetAllPaymentsByUserID(?)}");
			int n = 0;
			stmt.setInt(++n, pUserID);
			ResultSet rsPayment = stmt.executeQuery();

			while (rsPayment.next()) {
				Payment myPayment = new Payment();
				myPayment.setPaymentID(rsPayment.getInt("PaymentID"));
				myPayment.setAddressID(rsPayment.getInt("AddressID"));
				myPayment.setCardNumber(rsPayment.getString("CardNumber"));
				myPayment.setExpDate(rsPayment.getDate("ExpDate"));
				myPayment.setCvv(rsPayment.getString("CVV"));
				myPayment.setCardType(rsPayment.getString("CardType"));
				myPayment.setLastFour(rsPayment.getString("LastFour"));
				myPayment.setActive(rsPayment.getString("Active"));
				allUserPayments.add(myPayment);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allUserPayments;
	}

}
