package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PublicationDao implements Dao<Publication>{
	Properties appProps = new Properties();
	
	public PublicationDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public Publication get(int pPublicationID) {
		Publication returnPublication = new Publication();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetPublisherByPublicationID(?)}");
			int n = 0;
			stmt.setInt(++n, pPublicationID);
			ResultSet rsPublication = stmt.executeQuery();

			if (rsPublication.next()) {
				returnPublication.setPublicationID(rsPublication.getInt("PublicationID"));
				returnPublication.setAddressID(rsPublication.getInt("AddressID"));
				returnPublication.setPublisherName(rsPublication.getString("PublisherName"));
				returnPublication.setContactName(rsPublication.getString("ContactName"));
				returnPublication.setPhoneID(rsPublication.getInt("PhoneID"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnPublication;
	}
	
	@Override
	public List<Publication> getALL() {
		List<Publication> allPublications = new ArrayList<>();

		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetAllPublications()}");
			ResultSet rsPublication = stmt.executeQuery();
			while (rsPublication.next()) {
				Publication myPublication = new Publication();
				myPublication.setPublicationID(rsPublication.getInt("PublicationID"));
				myPublication.setAddressID(rsPublication.getInt("AddressID"));
				myPublication.setPublisherName(rsPublication.getString("PublisherName"));
				myPublication.setContactName(rsPublication.getString("ContactName"));
				myPublication.setPhoneID(rsPublication.getInt("PhoneID"));
				
				allPublications.add(myPublication);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allPublications;
	}
	
/*
	

	@Override
	public List<Publication> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	

	

	@Override
	public void delete(Publication objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeleteBookByISBN(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getISBN());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	*/

	//Trash v v v v v
	@Override
	public void create(Publication objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"), 
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreatePublisher(?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToSave.getAddressID());
			stmt.setString(++n, objectToSave.getPublisherName());
			stmt.setString(++n, objectToSave.getContactName());
			stmt.setDouble(++n, objectToSave.getPhoneID());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(Publication objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdatePublisherByPublisherID(?,?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getPublicationID());
			stmt.setInt(++n, objectToUpdate.getAddressID());
			stmt.setString(++n, objectToUpdate.getPublisherName());
			stmt.setString(++n, objectToUpdate.getContactName());
			stmt.setInt(++n, objectToUpdate.getPhoneID());
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1 ) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	@Override
	public void delete(Publication objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"), 
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call DeletePublisherByPublicationID(?)}");
			int n = 0;
			stmt.setInt(++n, objectToSave.getPublicationID());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		
	}
}