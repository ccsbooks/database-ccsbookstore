package model;

import java.time.LocalDate;

public class Publication {
	// from the publisher table
	/**
	 * This is the addressID.
	 */
	private int addressID;
	/**
	 * This is the publication name.
	 */
	private String publisherName;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Publication [addressID=" + addressID + ", publisherName=" + publisherName + ", contactName="
				+ contactName + ", phoneID=" + phoneID + ", copyrightDate=" + copyrightDate + "]";
	}

	/**
	 * This is the contactName.
	 */
	private String contactName;
	/**
	 * This is the PublicationID
	 */
	private int publicationID;
	/**
	 * This is the phoneID.
	 */
	private int phoneID;
	/**
	 * This is the copyright date.
	 */
	private LocalDate copyrightDate;

	/**
	 * @return the addressID
	 */
	public final int getAddressID() {
		return addressID;
	}

	/**
	 * @return the publicationID
	 */
	public final int getPublicationID() {
		return publicationID;
	}

	/**
	 * @param publicationID
	 *            the publicationID to set
	 */
	public final void setPublicationID(int publicationID) {
		this.publicationID = publicationID;
	}

	/**
	 * @param addressID
	 *            the addressID to set
	 */
	public final void setAddressID(final int addressID) {
		this.addressID = addressID;
	}

	/**
	 * @return the publisherName
	 */
	public final String getPublisherName() {
		return publisherName;
	}

	/**
	 * @param publisherName
	 *            the publisherName to set
	 */
	public final void setPublisherName(final String publisherName) {
		this.publisherName = publisherName;
	}

	/**
	 * @return the contactName
	 */
	public final String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName
	 *            the contactName to set
	 */
	public final void setContactName(final String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the phoneID
	 */
	public final int getPhoneID() {
		return phoneID;
	}

	/**
	 * @param phoneID
	 *            the phoneID to set
	 */
	public final void setPhoneID(final int phoneId) {
		this.phoneID = phoneId;
	}

	/**
	 * @return the copyrightDate
	 */
	public final LocalDate getCopyrightDate() {
		return copyrightDate;
	}

	/**
	 * @param copyrightDate
	 *            the copyrightDate to set
	 */
	public final void setCopyrightDate(final LocalDate copyrightDate) {
		this.copyrightDate = copyrightDate;
	}

}
