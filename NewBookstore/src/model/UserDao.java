package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UserDao implements Dao<User> {
	Properties appProps = new Properties();
	private List<CartItem> shoppingCartItems = new ArrayList<>();

	public List<CartItem> getAllShoppingCartItems() {
		return shoppingCartItems;
	}

	public UserDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // this is a constructor for the UserDao.java class

	// Once all of the properties are set in
	// the business logic for a user,
	// the DAO sends the data to the database.
	// DAO = Data Access Object, provides an
	// abstract interface to a database.
	@Override
	public void create(User objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call CreateUser(?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getLastName());
			stmt.setString(++n, objectToSave.getFirstName());
			stmt.setString(++n, objectToSave.getPassword());
			stmt.setString(++n, objectToSave.getEmail());
			stmt.setString(++n, objectToSave.getPhone().getPhoneNumber());
			stmt.setString(++n, objectToSave.getPhone().getPhoneType());
			stmt.setString(++n, "T");
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<User> getALL() {
		List<User> allUsers = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			
			CallableStatement stmt = cn.prepareCall("{call GetAllUsers()}");
			ResultSet rsUser = stmt.executeQuery();
			while (rsUser.next()) {
				User myUser = new User();
				myUser.setUserID(rsUser.getInt("UserID"));
				myUser.setLastName(rsUser.getString("LastName"));
				myUser.setFirstName(rsUser.getString("FirstName"));
				myUser.setPassword(rsUser.getString("Password"));
				myUser.setEmail(rsUser.getString("Email"));
				myUser.setAdmin(rsUser.getInt("Admin"));
				myUser.setActive(rsUser.getString("Active"));
				allUsers.add(myUser);
				
			}
			return allUsers;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	@Override
	public User get(int id) {
		return null;
	}

	@Override
	public void update(User objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdateUserByUserID(?,?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getUserID());
			stmt.setString(++n, objectToUpdate.getLastName());
			stmt.setString(++n, objectToUpdate.getFirstName());
			stmt.setString(++n, objectToUpdate.getEmail());
			stmt.setString(++n, objectToUpdate.getPhone().getPhoneNumber());
			stmt.executeQuery();
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(User objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeleteUserByUserID(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getUserID());
			stmt.setString(++n, "f");
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public User get(String pEmail, String pPassword) {
		User loadUser = new User();
		String generatedPassword = null;
		try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(pPassword.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        
        
		// List<ShoppingCart> userShoppingCart = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetUserByEmailAndPassword(?,?)}");
			int n = 0;
			stmt.setString(++n, pEmail);
			stmt.setString(++n, generatedPassword);
			ResultSet rsUser = stmt.executeQuery();

			// int pUserId = 0;
			if (rsUser.next()) {
				loadUser.setUserID(rsUser.getInt("UserID"));
				// pUserId = rsUser.getInt("UserID");
				loadUser.setLastName(rsUser.getString("LastName"));
				loadUser.setFirstName(rsUser.getString("FirstName"));
				loadUser.setPassword(rsUser.getString("Password"));
				loadUser.setEmail(rsUser.getString("Email"));
				loadUser.setAdmin(rsUser.getInt("Admin"));
				loadUser.setActive(rsUser.getString("Active"));
				loadUser.getPhone().setPhoneType(rsUser.getString("PhoneType"));
				loadUser.getPhone().setPhoneNumber(rsUser.getString("PhoneNumber"));
			}

			// CallableStatement stmtTwo = cn.prepareCall("{call
			// GetUserShoppingCartItemsByUserID(?)}");
			// int m = 0;
			// stmtTwo.setInt(++m, pUserId);
			// ResultSet rsUserShoppingCart = stmtTwo.executeQuery();
			//
			// while (rsUserShoppingCart.next()) {
			// ShoppingCart shoppingCartItem= new ShoppingCart();
			// shoppingCartItem.setBookId(rsUserShoppingCart.getInt("BookID"));
			// shoppingCartItem.setQuantity(rsUserShoppingCart.getInt("Quantity"));
			// userShoppingCart.add(shoppingCartItem);
			// }
			// loadUser.setShoppingCart(userShoppingCart);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return loadUser;
	}

	public List<User> getAllbyID(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void addToShoppingCart(CartItem objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call CreateUserCartItem(?,?,?)}");

			int n = 0;
			stmt.setInt(++n, objectToSave.getUserId());
			stmt.setInt(++n, objectToSave.getBookID());
			stmt.setInt(++n, objectToSave.getQuantity());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteUserCartItem(CartItem objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeleteUserCartItem(?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getUserId());
			stmt.setInt(++n, objectToDelete.getBookID());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void updateUserCartItem(CartItem objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdateUserCartItem(?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getUserId());
			stmt.setInt(++n, objectToUpdate.getBookID());
			stmt.setInt(++n, objectToUpdate.getQuantity());
			stmt.executeQuery();
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public CartItem getShoppingCartItem(int pUserId, int pBookId) {
		CartItem returnSc = new CartItem();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetShoppingCartItemByUserIDAndBookID(?,?)}");
			int n = 0;
			stmt.setInt(++n, pUserId);
			stmt.setInt(++n, pBookId);
			ResultSet rsSc = stmt.executeQuery();

			if (rsSc.next()) {
				returnSc.setUserId(rsSc.getInt("UserID"));
				returnSc.setBookID(rsSc.getInt("BookID"));
				returnSc.setQuantity(rsSc.getInt("Quantity"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnSc;
	}

	public List<CartItem> getShoppingCartItemsByUserId(int pUserId) {
		List<CartItem> userShoppingCart = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetUserShoppingCartItemsByUserID(?)}");
			int m = 0;
			stmt.setInt(++m, pUserId);
			ResultSet rsUserShoppingCart = stmt.executeQuery();

			while (rsUserShoppingCart.next()) {
				CartItem shoppingCartItem = new CartItem();
				shoppingCartItem.setUserId(rsUserShoppingCart.getInt("UserID"));
				shoppingCartItem.setBookID(rsUserShoppingCart.getInt("BookID"));
				shoppingCartItem.setQuantity(rsUserShoppingCart.getInt("Quantity"));
				userShoppingCart.add(shoppingCartItem);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userShoppingCart;
	}
}
