package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class PhoneDao implements Dao<Phone> {
	Properties appProps = new Properties();

	public PhoneDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // this is a constructor for the AddressDao.java class

	public void createForUser(Phone objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call CreateBook(?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getPhoneNumber());
			stmt.setString(++n, objectToSave.getPhoneType());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	@Override
	public List<Phone> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Phone> getAllbyID(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Phone get(int pPhone) {
		Phone returnPhone = new Phone();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetPhoneByPhoneID(?)}");
			int n = 0;
			stmt.setInt(++n, pPhone);
			ResultSet rsPhone = stmt.executeQuery();

			if (rsPhone.next()) {
				returnPhone.setPhoneID(rsPhone.getInt("PhoneID"));
				returnPhone.setPhoneType(rsPhone.getString("PhoneType"));
				returnPhone.setPhoneNumber(rsPhone.getString("PhoneNumber"));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnPhone;
	}

	@Override
	public void update(Phone objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdatePhoneByPhoneID(?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getPhoneID());
			stmt.setString(++n, objectToUpdate.getPhoneType());
			stmt.setString(++n, objectToUpdate.getPhoneNumber());
			stmt.executeQuery();
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1 ) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void delete(Phone objectToDelete) {
		// TODO Auto-generated method stub

	}
	private int phoneID;
	public int createForBook(Phone myPhone) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreatePhone(?,?)}");
			
			int n = 0;
			stmt.setString(++n, myPhone.getPhoneType());
			stmt.setString(++n, myPhone.getPhoneNumber());
			ResultSet rsAddress = stmt.executeQuery();
			if (rsAddress.next()) {
			phoneID = rsAddress.getInt("PhoneID");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return phoneID;
	
	}

	@Override
	public void create(Phone objectToSave) {
		// TODO Auto-generated method stub
		
	}

}
