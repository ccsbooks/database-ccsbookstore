package model;

import java.util.ArrayList;
import java.util.List;

public class Address {
	/**
	 * Address needs a userID because a user "has-an" address
	 * so this ID will be used to load specific addresses that
	 * the user needs from the database
	 */
	private int userID;
	private int addressID;
	private String addressLineOne;
	private String addressLineTwo;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String addressLastName;
	private String addressFirstName;
	private String active;
	private List<Address> userAddresses = new ArrayList<Address>();
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	public String getAddressLineOne() {
		return addressLineOne;
	}
	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}
	public String getAddressLineTwo() {
		return addressLineTwo;
	}
	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddressLastName() {
		return addressLastName;
	}
	public void setAddressLastName(String addressLastName) {
		this.addressLastName = addressLastName;
	}
	public String getAddressFirstName() {
		return addressFirstName;
	}
	public void setAddressFirstName(String addressFirstName) {
		this.addressFirstName = addressFirstName;
	}
	public int getAddressID() {
		return addressID;
	}
	public void setAddressID(int addressID) {
		this.addressID = addressID;
	}
	public List<Address> getUserAddresses() {
		return userAddresses;
	}
	public void setUserAddresses(List<Address> userAddresses2) {
		this.userAddresses = userAddresses2;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	

}
