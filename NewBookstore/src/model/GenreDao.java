package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GenreDao implements Dao<Genre> {
	Properties appProps = new Properties();

	public GenreDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // this is a constructor for the AddressDao.java class

	@Override
	public void create(Genre objectToSave) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Genre> getALL() {
		List<Genre> allGenres = new ArrayList<>();

		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetAllGenres()}");
			ResultSet rsGenre = stmt.executeQuery();
			while (rsGenre.next()) {
				Genre myGenre = new Genre();
				myGenre.setGenreID(rsGenre.getInt("GenreID"));
				myGenre.setGenreName(rsGenre.getString("GenreName"));
				
				allGenres.add(myGenre);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allGenres;
	}

	public List<Phone> getAllbyID(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Genre get(int pGenre) {
		return null;
		
	}

	public Phone get(String email, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Genre objectToUpdate) {
		/*
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdatePhoneByPhoneID(?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getPhoneID());
			stmt.setString(++n, objectToUpdate.getPhoneType());
			stmt.setString(++n, objectToUpdate.getPhoneNumber());
			stmt.executeQuery();
			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1 ) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

*/
	}

	@Override
	public void delete(Genre objectToSave) {
		// TODO Auto-generated method stub

	}

	public int createForBook(Genre myGenre) {
		return 0;
		/*
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))){
			
			CallableStatement stmt = cn.prepareCall("{call CreatePhone(?,?)}");
			
			int n = 0;
			stmt.setString(++n, myPhone.getPhoneType());
			stmt.setString(++n, myPhone.getPhoneNumber());
			ResultSet rsAddress = stmt.executeQuery();
			if (rsAddress.next()) {
			phoneID = rsAddress.getInt("PhoneID");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return phoneID;
	*/
	}

}
