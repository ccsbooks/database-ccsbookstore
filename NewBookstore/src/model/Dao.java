package model;

import java.util.List;

public interface Dao<T> {
	public void create (T objectToSave);
	public List<T> getALL();
	
	public T get(int id);
	
	public void update(T objectToSave);
	public void delete(T objectToSave);
	
}

