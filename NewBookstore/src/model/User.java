package model;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to create a user in the database.
 * @author Caleb
 *
 */
public class User {
	/**
 	* lastName variable
 	*/
	private String lastName;
	/**
	 * Admin variable
	 */
	private int admin;
	/**
	 * firstName variable
	 */
	private String firstName;
	/**
	 * the password will be a string variable
	 */
	private String password;
	/**
	 * the email will be a string variable
	 */
	private String email;
	/**
	 * This variable will be a place holder for
	 * the ID for the user being grabbed from the
	 * database.
	 */
	private int userID;
	/**
	 * This determines whether the user
	 * has been "deleted" from the database or not
	 */
	private String active;
	/**
	 * 
	 * This is the User shopping cart
	 */
	private List<CartItem> shoppingCartItems = new ArrayList<CartItem>();
	/**
	 * Aggregate the users phone inside the user class
	 */
	private Phone phone = new Phone();
	/**
	 * @return the admin
	 */
	public final int getAdmin() {
		return admin;
	}
	/**
	 * @param admin the admin to set
	 */
	public final void setAdmin(int admin) {
		this.admin = admin;
	}
	/**
	 * getter for the lastName
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * setter for the lastName
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * getter for the firstName
	 * @return firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * setter for the firstName
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * getter for the password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * setter for the password
	 * @param password
	 */
	public void setPassword(String password) {
	
		String generatedPassword = null;
		try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
		
		this.password = generatedPassword;
	}
	/**
	 * getter for the email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * setter for the email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * getter for the userID
	 * @return
	 */
	public int getUserID() {
		return userID;
	}
	/**
	 * setter for the userID
	 * @param userID
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public List<CartItem> getShoppingCart() {
		return shoppingCartItems;
	}
	public void setShoppingCart(List<CartItem> shoppingCart) {
		this.shoppingCartItems = shoppingCart;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public Phone getPhone() {
		return phone;
	}
	public void setPhone(Phone phone) {
		this.phone = phone;
	}
	
	
}
