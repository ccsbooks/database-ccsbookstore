package model;

import java.util.Date;

public class Purchase {
	private int userId;
	private Date purchaseDate;
	private Address purchAddress;
	private Payment purchPayment;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getPurchaseDate() {
		return purchaseDate;
	}
	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	public Address getPurchAddress() {
		return purchAddress;
	}
	public void setPurchAddress(Address purchAddress) {
		this.purchAddress = purchAddress;
		
	}
	public Payment getPurchPayment() {
		return purchPayment;
	}
	public void setPurchPayment(Payment purchPayment) {
		this.purchPayment = purchPayment;
	}

}
