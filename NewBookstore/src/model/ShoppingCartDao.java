package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class ShoppingCartDao implements Dao<CartItem> {
	Properties appProps = new Properties();
	public ShoppingCartDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
			getALL();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void create(CartItem objectToSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<CartItem> getALL() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CartItem get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(CartItem objectToSave) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(CartItem objectToSave) {
		// TODO Auto-generated method stub
		
	}

	public int placeOrder(int pUserID, int pPamentID, int pAddressID, int pDiscount) throws SQLException {
		Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"));
			
			CallableStatement stmt = cn.prepareCall("{call PlaceOrder(?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, pUserID);
			stmt.setInt(++n, pPamentID);
			stmt.setInt(++n, pAddressID);
			stmt.setInt(++n, pDiscount);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
			return rs.getInt("purchaseID");
			}
			return 0;
		

	}
}
