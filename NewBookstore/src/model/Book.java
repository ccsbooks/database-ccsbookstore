package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the class for the books table in the Database.
 * @author 174843
 *
 */
public class Book {
	//from the book table
	/**
	 * This is the bookID.
	 */
	private int bookID;
	/**
	 * This is the ISBN.
	 */
	private String iSBN;
	
	/**
	 * This is the publicationID.
	 */
	private int publicationID;
	/**
	 * This is the bookName.
	 */
	private String bookName;
	/**
	 * This is the author.
	 */
	private String author;
	/**
	 * This is the retailPrice.
	 */
	private double retailPrice;
	/**
	 * This is the binding.
	 */
	private String binding;
	/**
	 * This is the quantityONHand.
	 */
	private int quantityOnHand;
	/**
	 * This is the wholesale price.
	 */
	private double wholesale;
	/**
	 * This is the Publication Date.
	 */
	private LocalDate publicationDate;
	//from the genre table
	/**
	 * This is the genreID.
	 */
	private int genreID;
	
	public Publication myPublication = new Publication();
	public PublicationDao daoPublication = new PublicationDao();
	
	
	//TODO: fix this to be an enum.
	/**
	 * This is the genre.
	 */
	private List<Genre> genres = new ArrayList<>();
	/**
	 * @return the bookID
	 */
	public final int getBookID() {
		return bookID;
	}
	/**
	 * @param bookID the bookID to set
	 */
	public final void setBookID(final int bookID) {
		this.bookID = bookID;
	}
	/**
	 * @return the iSBN
	 */
	public final String getISBN() {
		return iSBN;
	}
	/**
	 * @param iSBN the iSBN to set
	 */
	public final void setISBN(final String iSBN) {
		this.iSBN = iSBN;
	}
	/**
	 * @return the publicationID
	 */
	public final int getPublicationID() {
		return publicationID;
	}
	/**
	 * @param publicationID the publicationID to set
	 */
	public final void setPublicationID(final int publicationID) {
		this.publicationID = publicationID;
	}
	/**
	 * @return the bookName
	 */
	public final String getBookName() {
		return bookName;
	}
	/**
	 * @param bookName the bookName to set
	 */
	public final void setBookName(final String bookName) {
		this.bookName = bookName;
	}
	/**
	 * @return the author
	 */
	public final String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public final void setAuthor(final String author) {
		this.author = author;
	}
	/**
	 * @return the retailPrice
	 */
	public final double getRetailPrice() {
		return retailPrice;
	}
	/**
	 * @param retailPrice the retailPrice to set
	 */
	public final void setRetailPrice(final double retailPrice) {
		this.retailPrice = retailPrice;
	}
	/**
	 * @return the binding
	 */
	public final String getBinding() {
		return binding;
	}
	/**
	 * @param binding the binding to set
	 */
	public final void setBinding(final String binding) {
		this.binding = binding;
	}
	/**
	 * @return the quantityOnHand
	 */
	public final int getQuantityOnHand() {
		return quantityOnHand;
	}
	/**
	 * @param quantityOnHand the quantityOnHand to set
	 */
	public final void setQuantityOnHand(final int quantityOnHand) {
		this.quantityOnHand = quantityOnHand;
	}
	/**
	 * @return the genreID
	 */
	public final int getGenreID() {
		return genreID;
	}
	/**
	 * @param genreID the genreID to set
	 */
	public final void setGenreID(final int genreID) {
		this.genreID = genreID;
	}
	/**
	 * @return the wholesale
	 */
	public final double getWholesale() {
		return wholesale;
	}
	/**
	 * @param wholesale the wholesale to set
	 */
	public final void setWholesale(final double wholesale) {
		this.wholesale = wholesale;
	}
	/**
	 * @return the genre
	 */
	public final List<Genre> getGenre() {
		return genres;
	}
	/**
	 * @return the genreName
	 */
	public final String getGenreName() {
		try {
		return genres.get(0).getGenreName();
		} catch (Exception e) {
			return "Not Added";
		}
	}
	public final void setGenre(final ArrayList<Genre> pGenres) {
		genres = pGenres;
	}
	/**
	 * @param genre the genre to add
	 */
	public final void addGenre(final Genre genre) {
		this.genres.add(genre);
	}
	/**
	 * @param genre the genre to remove
	 */
	public final void removeGenre(final Genre genre) {
		this.genres.remove(genre);
	}
	/**
	 * @return the publicationDate
	 */
	public final LocalDate getPublicationDate() {
		return publicationDate;
	}
	/**
	 * @param publicationDate the publicationDate to set
	 */
	public final void setPublicationDate(LocalDate publicationDate) {
		this.publicationDate = publicationDate;
	}
	
}
