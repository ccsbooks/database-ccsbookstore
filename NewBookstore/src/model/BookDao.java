package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class BookDao implements Dao<Book> {
	Properties appProps = new Properties();

	public List<Book> getAllBooks() {
		return getALL();
	}

	public BookDao() {
		try {
			appProps.load(new FileInputStream("app.config"));
			getALL();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void create(Book objectToSave) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call CreateBook(?,?,?,?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setString(++n, objectToSave.getISBN());
			stmt.setInt(++n, objectToSave.getPublicationID());
			stmt.setString(++n, objectToSave.getBookName());
			stmt.setString(++n, objectToSave.getAuthor());
			stmt.setDouble(++n, objectToSave.getRetailPrice());
			stmt.setString(++n, objectToSave.getBinding());
			stmt.setInt(++n, objectToSave.getQuantityOnHand());
			stmt.setDouble(++n, objectToSave.getWholesale());
			stmt.setDate(++n, java.sql.Date.valueOf(objectToSave.getPublicationDate()));
			stmt.setInt(++n, objectToSave.getGenreID());
			stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// TODO: Finish the get book by ISBN
	@Override
	public Book get(int pBookID) {
		Book returnBook = new Book();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetBookByBookID(?)}");
			int n = 0;
			stmt.setInt(++n, pBookID);
			ResultSet rsBook = stmt.executeQuery();

			if (rsBook.next()) {
				returnBook.setBookID(rsBook.getInt("BookID"));
				returnBook.setISBN(rsBook.getString("ISBN"));
				returnBook.setPublicationID(rsBook.getInt("PublicationID"));
				returnBook.setBookName(rsBook.getString("BookName"));
				returnBook.setAuthor(rsBook.getString("Author"));
				returnBook.setRetailPrice(rsBook.getDouble("RetailPrice"));
				returnBook.setBinding(rsBook.getString("Binding"));
				returnBook.setQuantityOnHand(rsBook.getInt("QuantityOnHand"));
				returnBook.setWholesale(rsBook.getDouble("Wholesale"));
				returnBook.setPublicationDate(LocalDate.parse(rsBook.getString("PublicationDate")));
				String genreID = rsBook.getString("GenreIDs");
				String genreName = rsBook.getString("GenreNames");
				if (genreID != null) {
					String[] gIDs = genreID.split(",", -1);
					returnBook.setGenreID(Integer.parseInt(gIDs[0]));
					String[] gNames = genreName.split(",", -1);
					for (int i = 0; i < gIDs.length; i++) {
						Genre g = new Genre(Integer.parseInt(gIDs[i]), gNames[i]);

						returnBook.addGenre(g);
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnBook;
	}

	public Book getByISBN(int pISBN) {
		Book returnBook = new Book();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetBookByBookISBN(?)}");
			int n = 0;
			stmt.setInt(++n, pISBN);
			ResultSet rsBook = stmt.executeQuery();

			if (rsBook.next()) {
				returnBook.setBookID(rsBook.getInt("BookID"));
				returnBook.setISBN(rsBook.getString("ISBN"));
				returnBook.setPublicationID(rsBook.getInt("PublicationID"));
				returnBook.setBookName(rsBook.getString("BookName"));
				returnBook.setAuthor(rsBook.getString("Author"));
				returnBook.setRetailPrice(rsBook.getDouble("RetailPrice"));
				returnBook.setBinding(rsBook.getString("Binding"));
				returnBook.setQuantityOnHand(rsBook.getInt("QuantityOnHand"));
				returnBook.setWholesale(rsBook.getDouble("Wholesale"));
				returnBook.setPublicationDate(LocalDate.parse(rsBook.getString("PublicationDate")));
				String genreID = rsBook.getString("GenreIDs");
				String genreName = rsBook.getString("GenreNames");
				if (genreID != null) {
					String[] gIDs = genreID.split(",", -1);
					String[] gNames = genreName.split(",", -1);
					for (int i = 0; i < gIDs.length; i++) {
						Genre g = new Genre(Integer.parseInt(gIDs[i]), gNames[i]);

						returnBook.addGenre(g);
					}
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnBook;
	}

	@Override
	public List<Book> getALL() {
		List<Book> allBooks = new ArrayList<>();

		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call GetAllBooks()}");
			ResultSet rsBook = stmt.executeQuery();
			while (rsBook.next()) {
				Book myBook = new Book();
				myBook.setBookID(rsBook.getInt("BookID"));
				myBook.setISBN(rsBook.getString("ISBN"));
				myBook.setPublicationID(rsBook.getInt("PublicationID"));
				myBook.setBookName(rsBook.getString("BookName"));
				myBook.setAuthor(rsBook.getString("Author"));
				myBook.setRetailPrice(rsBook.getDouble("RetailPrice"));
				myBook.setBinding(rsBook.getString("Binding"));
				myBook.setQuantityOnHand(rsBook.getInt("QuantityOnHand"));
				myBook.setWholesale(rsBook.getDouble("Wholesale"));
				myBook.setPublicationDate(LocalDate.parse(rsBook.getString("PublicationDate")));
				String genreID = rsBook.getString("GenreIDs");
				String genreName = rsBook.getString("GenreNames");
				if (genreID != null) {
					String[] gIDs = genreID.split(",", -1);
					myBook.setGenreID(Integer.parseInt(gIDs[0]));
					String[] gNames = genreName.split(",", -1);
					for (int i = 0; i < gIDs.length; i++) {
						Genre g = new Genre(Integer.parseInt(gIDs[i]), gNames[i]);
						myBook.addGenre(g);
					}
				}
				allBooks.add(myBook);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allBooks;
	}

	public List<Book> searchAll(String pSearch) {

		List<Book> allBooks = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			CallableStatement stmt = cn.prepareCall("{call SearchAll(?)}");
			int n = 0;
			stmt.setString(++n, pSearch);
			ResultSet rsBook = stmt.executeQuery();

			while /* comment break */ (rsBook.next()) {
				Book returnBook = new Book();
				returnBook.setBookID(rsBook.getInt("BookID"));
				returnBook.setISBN(rsBook.getString("ISBN"));
				returnBook.setPublicationID(rsBook.getInt("PublicationID"));
				returnBook.setBookName(rsBook.getString("BookName"));
				returnBook.setAuthor(rsBook.getString("Author"));
				returnBook.setRetailPrice(rsBook.getDouble("RetailPrice"));
				returnBook.setBinding(rsBook.getString("Binding"));
				returnBook.setQuantityOnHand(rsBook.getInt("QuantityOnHand"));
				returnBook.setWholesale(rsBook.getDouble("Wholesale"));
				returnBook.setPublicationDate(LocalDate.parse(rsBook.getString("PublicationDate")));
				String genreID = rsBook.getString("GenreIDs");
				String genreName = rsBook.getString("GenreNames");
				if (genreID != null) {
					String[] gIDs = genreID.split(",", -1);
					String[] gNames = genreName.split(",", -1);
					for (int i = 0; i < gIDs.length; i++) {
						Genre g = new Genre(Integer.parseInt(gIDs[i]), gNames[i]);
						returnBook.addGenre(g);
					}
				}
				allBooks.add(returnBook);
				allBooks.get(0).getBookID();
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return allBooks;
	}

	public List<Book> searchAllByBestSellers() {

		List<Book> allBooks = new ArrayList<>();
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {
			CallableStatement stmt = cn.prepareCall("{call SearchAllByBestSellers()}");
			ResultSet rsBook = stmt.executeQuery();

			while /* comment break */ (rsBook.next()) {
				Book returnBook = new Book();
				returnBook.setBookID(rsBook.getInt("BookID"));
				returnBook.setISBN(rsBook.getString("ISBN"));
				returnBook.setPublicationID(rsBook.getInt("PublicationID"));
				returnBook.setBookName(rsBook.getString("BookName"));
				returnBook.setAuthor(rsBook.getString("Author"));
				returnBook.setRetailPrice(rsBook.getDouble("RetailPrice"));
				returnBook.setBinding(rsBook.getString("Binding"));
				returnBook.setQuantityOnHand(rsBook.getInt("QuantityOnHand"));
				returnBook.setWholesale(rsBook.getDouble("Wholesale"));
				returnBook.setPublicationDate(LocalDate.parse(rsBook.getString("PublicationDate")));
				String genreID = rsBook.getString("GenreIDs");
				String genreName = rsBook.getString("GenreNames");
				if (genreID != null) {
					String[] gIDs = genreID.split(",", -1);
					String[] gNames = genreName.split(",", -1);
					for (int i = 0; i < gIDs.length; i++) {
						Genre g = new Genre(Integer.parseInt(gIDs[i]), gNames[i]);
						returnBook.addGenre(g);
					}
				}
				allBooks.add(returnBook);
				allBooks.get(0).getBookID();
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}

		return allBooks;
	}
	
	@Override
	public void update(Book objectToUpdate) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call UpdateBookByBookID(?,?,?,?,?,?,?,?,?,?,?)}");
			int n = 0;
			stmt.setInt(++n, objectToUpdate.getBookID());
			stmt.setString(++n, objectToUpdate.getISBN());
			stmt.setInt(++n, objectToUpdate.getPublicationID());
			stmt.setString(++n, objectToUpdate.getBookName());
			stmt.setString(++n, objectToUpdate.getAuthor());
			stmt.setDouble(++n, objectToUpdate.getRetailPrice());
			stmt.setString(++n, objectToUpdate.getBinding());
			stmt.setInt(++n, objectToUpdate.getQuantityOnHand());
			stmt.setDouble(++n, objectToUpdate.getWholesale());
			stmt.setDate(++n, java.sql.Date.valueOf(objectToUpdate.getPublicationDate()));
			stmt.setInt(++n, objectToUpdate.getGenreID());

			int rowsAffected = stmt.executeUpdate();
			if (rowsAffected != 1) {
				throw new InvalidParameterException("This update affected " + rowsAffected + " rows.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(Book objectToDelete) {
		try (Connection cn = DriverManager.getConnection(appProps.getProperty("jdbcConnection"),
				appProps.getProperty("userName"), appProps.getProperty("password"))) {

			CallableStatement stmt = cn.prepareCall("{call DeleteBookByBookID(?)}");
			int n = 0;
			stmt.setInt(++n, objectToDelete.getBookID());
			stmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}